class Emergency < ActiveRecord::Base
  has_many :flights

  def as_json(options={})
    # Get emergency ID
    # emergency_id = super['id'].to_s

    # Find all photos with this ID
    # s3 = Aws::S3::Resource.new
    # bucket = s3.bucket("skripsie-uploads")
    # prefix = "emergencies/" + emergency_id
    # urls = bucket.objects(prefix: prefix)#.collect(&:public_url)

    # Append photos to JSON
    super.as_json(options).merge({flights: self.flights})
  end
end