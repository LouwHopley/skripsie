class Flight < ActiveRecord::Base
  belongs_to :drone
  belongs_to :emergency

  has_many :flight_entry_batteries
  has_many :flight_entry_locations
  has_many :flight_plan_entries


  def as_json(options={})
    fpe = self.flight_plan_entries.order(priority: :asc)
    fel = self.flight_entry_locations.order(created_at: :desc)
    feb = self.flight_entry_batteries.order(created_at: :desc)

    super.as_json(options).merge({drone: self.drone, flight_plan_entries: fpe, flight_entry_locations: fel, flight_entry_batteries: feb})
  end

end
