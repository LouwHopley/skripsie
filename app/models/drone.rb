class Drone < ActiveRecord::Base
  belongs_to :base_station
  has_many :flights

  def most_recent_flight
    self.flights('created_at DESC').limit(1).first
  end

  def as_json(options={})
    latest_flight = self.flights('created_at DESC').limit(1).first
    lat = nil
    lon = nil
    battery_level = 0
    if latest_flight
      latest_flight_entry_location = latest_flight.flight_entry_locations('created_at DESC').limit(1).first
      if latest_flight_entry_location
        lat = latest_flight_entry_location.latitude
        lon = latest_flight_entry_location.longitude
      end

      latest_flight_entry_battery = latest_flight.flight_entry_batteries('created_at DESC').limit(1).first
      if latest_flight_entry_battery
        battery_level = latest_flight_entry_battery.level
      end
    end
    super.as_json(options).merge({base_station: self.base_station, location: {lat: lat, lon: lon}, battery_level: battery_level})
  end

end
