class AppController < ApplicationController
  skip_before_action :verify_authenticity_token

  before_filter :cors_preflight_check
  after_filter :cors_set_access_control_headers

  # For all responses in this controller, return the CORS access control headers.

  def cors_set_access_control_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS'
    headers['Access-Control-Max-Age'] = "1728000"
  end

  # If this is a preflight OPTIONS request, then short-circuit the
  # request, return only the necessary headers and return an empty
  # text/plain.

  def cors_preflight_check
    if request.method == :options
      headers['Access-Control-Allow-Origin'] = '*'
      headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS'
      headers['Access-Control-Allow-Headers'] = 'X-Requested-With, X-Prototype-Version'
      headers['Access-Control-Max-Age'] = '1728000'
      render :text => '', :content_type => 'text/plain'
    end
  end

  # Create new emergency. Linked to this user
  def create
    puts "Creating new emergency."

    code = random_shortcode
    while Emergency.where(shortcode: code).count > 0 do
      puts "Shortcode", code, "taken. Trying again."
      code = random_shortcode
    end

    e = Emergency.new
    e.description = params[:description]
    e.latitude = params[:latitude]
    e.longitude = params[:longitude]
    e.caller_name = params[:name]
    e.caller_number = params[:number]
    e.services_required = params[:services_required]
    e.status = "pending"
    e.shortcode = code
    e.save!
    puts "Done."

    render json: e
  end

  def show
    id = params[:id]
    name = params[:name]
    number = params[:number]
    e = Emergency.find_by(id: id, caller_name: name, caller_number: number)
    render json: e
  end

  def all
    name = params[:name]
    number = params[:number]
    e = Emergency.where(caller_name: name, caller_number: number)
    puts "FINDING FINGDING"
    puts "FINDING FINGDING"
    puts "FINDING FINGDING"
    puts "FINDING FINGDING"
    puts name, number
    puts e
    render json: e
  end

  private
  def random_shortcode
    n = rand(10000)
    s = n.to_s
    if n < 10
      s = "000" + s
    elsif n < 100
      s = "00" + s
    elsif n < 1000
      s = "0" + s
    end
    return s
  end

end