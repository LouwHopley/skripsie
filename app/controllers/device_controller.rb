class DeviceController < ApplicationController
  skip_before_action :verify_authenticity_token

  # New device online to be added
  def subscribe
    # Pick random name
    bird_names = ["Turkey vulture","King vulture","Black vulture","California condor","Andean condor","Northern crested caracara","Pygmy falcon","Common kestrel","Yellow-headed caracara","Laughing falcon","Greater kestrel","Merlin","American kestrel","Eurasian hobby","Lanner falcon","Gyrfalcon","Prairie falcon","Peregrine falcon","Secretarybird","Osprey","European honey buzzard","Swallow-tailed kite","Snail kite","Mississippi kite","Red kite","White-tailed eagle","Black kite","Brahminy kite","African fish eagle","Bald eagle","Palm-nut vulture","Bearded vulture","Steller's sea eagle","Egyptian vulture","White-backed vulture","Indian vulture","Griffon vulture","Rüppell's vulture","Lappet-faced vulture","Black-chested snake eagle","Crested serpent eagle","Western marsh harrier","Bateleur","Pied harrier","African harrier-hawk","Northern harrier","Lizard buzzard","Spotted harrier","Grey goshawk","Dark chanting goshawk","Little sparrowhawk","Besra","Eurasian sparrowhawk","Cooper's hawk","Northern goshawk","Grasshopper buzzard","Crane hawk","Plumbeous hawk","Common black hawk","Savanna hawk","White hawk","Harris' hawk","Black-collared hawk","Solitary eagle","Red-shouldered hawk","Swainson's hawk","Galápagos hawk","Zone-tailed hawk","Red-tailed hawk","Common buzzard","Ferruginous hawk","Long-legged buzzard","Harpy eagle","Philippine eagle","Crested eagle","Indian black eagle","Greater spotted eagle","Eastern imperial eagle","Golden eagle","Wedge-tailed eagle","Verreaux's eagle","Wahlberg's eagle","Black-and-white hawk-eagle","Martial eagle","Philippine hawk-eagle","Crowned eagle"]
    name = bird_names[Random.rand(bird_names.length)]

    d = Drone.new
    d.name = name
    d.power_usage = 0
    d.speed = 0
    d.status = "ready"
    d.base_station = BaseStation.first
    d.save!
    render json: d
  end

  # Get latest flightplan if things changed
  def flightplan
    drone = Drone.find_by(id: params[:id])
    flight = drone.most_recent_flight

    if flight == nil
      puts "Flight does not exists."
    end

    # Check if this is the first request
    if flight != nil && flight.take_off_at == nil
      puts "Flight just started. Setting take off at time."
      flight.update_attributes take_off_at: DateTime.now
    end

    # Check if this is actually an active flight plan
    if flight != nil && flight.touch_down_at == nil
      render json: flight.flight_plan_entries
    else
      render json: {grounded: true}
    end

  end

  # Flight entries
  def update
    # Establish who's uploading
    drone = Drone.find_by(id: params[:id])
    flight = drone.most_recent_flight
    emergency = flight.emergency

    # Configure pusher connection
    require 'pusher'
    Pusher.url = "https://2b3556cbc526febaf37e:6e4b5838632baf4ea473@api.pusherapp.com/apps/147545"

    puts "UPDATING"
    params["_json"].each do |a|
      if a["activity"] == "location"
        fe = FlightEntryLocation.new
        fe.latitude = a["location"]["latitude"]
        fe.longitude = a["location"]["longitude"]
        fe.created_at = DateTime.parse(a["created_at"])
        fe.flight = flight
        fe.save!
        Pusher.trigger('emergency_'+emergency.id.to_s, 'drone_location', { drone_id: drone.id, flight_id: flight.id, lat: fe.latitude, lng: fe.longitude })
        puts "SAVED ACTIVITY: " + fe.to_json

      elsif a["activity"] == "battery"
        fe = FlightEntryBattery.new
        fe.level = a["percentage"]
        fe.created_at = DateTime.parse(a["created_at"])
        fe.flight = flight
        fe.save!
        Pusher.trigger('emergency_'+emergency.id.to_s, 'drone_battery', { drone_id: drone.id, flight_id: flight.id, level: fe.level })
        puts "SAVED ACTIVITY: " + fe.to_json

      elsif a["activity"] == "photo"
        fpe_id = a["flight_plan_entry_id"]
        # Do nothing here.

      elsif a["activity"] == "action_completed"
        fpe_id = a["flight_plan_entry_id"]
        fpe = FlightPlanEntry.find_by(id: fpe_id)
        fpe.update_attributes completed_at: DateTime.parse(a["created_at"])
      end
    end
    puts "DONE UPDATING"

    render json: {success: true}
  end

  # Images
  def upload
    # Create & store images
    puts "1) UPLOADING"
    # Establish who's uploading
    drone = Drone.find_by(id: params[:id])
    flight = drone.most_recent_flight
    emergency = flight.emergency

    # Configure pusher connection
    require 'pusher'
    Pusher.url = "https://2b3556cbc526febaf37e:6e4b5838632baf4ea473@api.pusherapp.com/apps/147545"


    s3 = Aws::S3::Resource.new
    bucket = s3.bucket("skripsie-uploads")
    directory = "emergencies/#{emergency.id.to_s}"
    puts "2) DIRECTORY: " + directory
    params.each do |k, v|
      puts "3.1) EACH: " + k
      if (k[-4..-1] || k) == ".jpg"
        aws_path = directory + "/" + k
        puts "3.2) ATTEMPTING: " + aws_path
        aws_obj = bucket.object(aws_path)
        aws_obj.upload_file(File.expand_path(params[k].tempfile), acl:'public-read')
        puts "3.3) SAVED\t" + k + "\t" + aws_obj.public_url
        Pusher.trigger('emergency_' + emergency.id.to_s, 'photo', { drone_id: drone.id, flight_id: flight.id, url: aws_obj.public_url })
      end
    end
    puts "4) DONE UPLOADING"

    render json: {success: true}
  end

end