class EmergenciesController < ApplicationController

  def index
    render json: Emergency.all
  end

  def create
    # Generate shortcode
    code = random_shortcode
    while Emergency.where(shortcode: code).count > 0 do
      code = random_shortcode
    end
    post_params[:shortcode] = code
    render json: Emergency.create(post_params)
  end

  def show
    e = Emergency.find(params[:id])

    puts "Starting Amazon request"
    s3 = Aws::S3::Resource.new
    puts "Amazon 1"
    bucket = s3.bucket("skripsie-uploads")
    puts "Amazon 2"
    prefix = "emergencies/" + params[:id].to_s
    puts "Amazon 3", prefix
    urls = bucket.objects(prefix: prefix).collect(&:public_url)
    puts "Amazon 4", urls
    urls.shift
    puts "Amazon 5", urls
    ej = e.to_json
    ep = JSON.parse(ej)
    ep[:photos] = urls
    puts "Amazon 6:", ep.to_json

    render json: ep
  end

  def photos
    s3 = Aws::S3::Resource.new
    bucket = s3.bucket("skripsie-uploads")
    prefix = "emergencies/" + params[:id].to_s
    urls = bucket.objects(prefix: prefix).collect(&:public_url)
    render json: urls
  end

  def status_update
    e = Emergency.find(params[:id])
    e.status = params[:status]
    e.save!
    render json: {success: true}
  end

  def file
    puts "FILING FILING FILING FILING FILING"
    puts "FILING FILING FILING FILING FILING"
    puts "FILING FILING FILING FILING FILING"
    puts "FILING FILING FILING FILING FILING"
    puts "FILING FILING FILING FILING FILING"
    e = Emergency.find(params[:id])
    d = Drone.find(params[:drone][:id])
    puts "Creating flight.", params
    puts "FILING FILING FILING FILING FILING"
    puts "Test 1", e
    puts "Test 2", params[:drone][:id]
    puts "Test 3", d
    f = Flight.new
    f.version = 1
    f.drone = d
    f.emergency = e
    f.save!

    puts "Creating flight entry location."
    fe = FlightEntryLocation.new
    fe.latitude = d.base_station.latitude
    fe.longitude = d.base_station.longitude
    fe.flight = f
    fe.save!

    puts "Creating objectives."
    params[:objectives].each_with_index do |o, i|
      puts "Objective",i,o
      fp = FlightPlanEntry.new
      fp.priority = i
      fp.latitude = o[:lat]
      fp.longitude = o[:lng]
      fp.action = o[:action]
      fp.flight = f
      fp.save!
    end
    puts "Done creating flight plan."

    render json: {success: true}
  end

  def show_from_shortcode
    sc = params[:shortcode]
    if Emergency.where(shortcode: sc).count > 0
      render json: Emergency.where(shortcode: sc).first
    else
      render json: {failed: true}
    end
  end

  private
  def post_params
    params.require(:emergency).permit!
  end

  private
  def random_shortcode
    n = rand(10000)
    s = n.to_s
    if n < 10
      s = "000" + s
    elsif n < 100
      s = "00" + s
    elsif n < 1000
      s = "0" + s
    end
    return s
  end

end
