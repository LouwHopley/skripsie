class BaseStationsController < ApplicationController

  def index
    render json: BaseStation.all
  end

  def show
    render json: BaseStation.find(params[:id])
  end

end
