class DronesController < ApplicationController

  def index
    render json: Drone.all
  end

  def create
    render json: Drone.create(post_params)
  end

  def show
    render json: Drone.find(params[:id])
  end

  def update
    d = Drone.find(params[:id])
    p = {speed: params[:speed], base_station_id: params[:base_station_id]}
    d.update_attributes p
    render json: {success: true}
  end

  private
  def post_params
    params.require(:drone).permit!
  end

end
