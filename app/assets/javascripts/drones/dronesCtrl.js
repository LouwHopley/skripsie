angular.module('droneDispatch')
.controller('DronesCtrl', [
'$scope',
'$state',
'$location',
'$stateParams',
'MapService',
'drones',
'drone',
'drone_view',
function($scope, $state, $location, $stateParams, MapService, drones, drone, drone_view){
  $scope.drones = drones.drones;
  $scope.drone = drone;
  $scope.MapService = MapService;

  $scope.openDrone = function(drone) {
    console.log("Drone opening")
    $location.path('/drones/' + drone.id);
  };
  $scope.editDrone = function(drone) {
    $scope.editingDrone = drone;
    $location.path('/drones/' + drone.id + '/edit');
  };
  $scope.saveDrone = function(editDrone) {
    drones.update($scope.drone.id, editDrone).then(function(success){
      alert("Drone updated!");
      $location.path('/drones/' + drone.id);
    });
  };

  $scope.markers = [];
  if(drone_view)
  {
    $scope.markers = MapService.basesToMarkers([drone.base_station]);
    $scope.home_location = angular.copy($scope.markers[0]);
    $scope.markers = $scope.markers.concat(MapService.dronesToMarkers([drone]));
    console.log("BS:", drone.base_station);
    console.log("HL:", $scope.markers[0])
    console.log("M:", $scope.markers)
  }

}]);