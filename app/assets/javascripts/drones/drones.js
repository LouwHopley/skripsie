angular.module('droneDispatch')
.factory('drones', [
  '$http',
  function($http){
    var o = { drones: [] };
    o.getAll = function() {
      return $http.get('/drones.json').success(function(data){
        angular.copy(data, o.drones);
      });
    };
    o.create = function(drone) {
      console.log("Creating drone...");
      return $http.post('/drones.json', drone).success(function(data){
        o.drones.push(data);
      });
    };
    o.get = function(id) {
      return $http.get('/drones/' + id + '.json').then(function(res){
        return res.data;
      });
    };
    o.update = function(id, attributes) {
      return $http.put('/drones/' + id + '.json', attributes).then(function(res){
        return res.data;
      });
    };
    return o;
  }
])
.factory('drone', [
  '$http',
  function($http){
    var o = { drone: [] };
    return o;
  }
])
.factory('base_stations', [
  '$http',
  function($http){
    var o = {bases: []};
    o.getAll = function(id) {
      return $http.get('/base_stations.json').success(function(data){
        angular.copy(data, o.bases);
      });
    };
    return o;
  }
]);