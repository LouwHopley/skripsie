angular.module('droneDispatch')
.factory('emergencies', [
  '$http',
  function($http){
    var o = { emergencies: [] };
    o.getAll = function() {
      return $http.get('/emergencies.json').success(function(data){
        angular.copy(data, o.emergencies);
      });
    };
    o.create = function(emergency) {
      console.log("Creating emergency...");
      return $http.post('/emergencies.json', emergency).success(function(data){
        o.emergencies.push(data);
        return data;
      });
    };
    o.get = function(id) {
      return $http.get('/emergencies/' + id + '.json').then(function(res){
        return res.data;
      });
    };
    o.getFromShortcode = function(shortcode) {
      return $http.get('/emergencies/shortcode/' + shortcode + '.json').then(function(res){
        return res.data;
      });
    };
    o.setEmergencyStatus = function(id, status) {
      console.log("Setting emergency status to "+status+"...")
      return $http.put('/emergencies/'+id+'/update/'+status+'.json');
    };
    o.fileFlightPlan = function(emergency, drone, objectives) {
      console.log("Filing...");
      return $http.post('/emergencies/'+emergency.id+'/flightplan/file.json', {"drone": drone, "objectives": objectives}).success(function(data){
        console.log("Done filing");
        return data;
      });
    };
    return o;
  }
])
.factory('emergency', [
  '$http',
  function($http){
    var o = { emergency: [] };
    return o;
  }
]);