angular.module('droneDispatch')
.controller('EmergenciesCtrl', [
'$rootScope',
'$scope',
'$state',
'$location',
'$stateParams',
'MapService',
'emergencies',
'emergency',
'base_stations',
function($rootScope, $scope, $state, $location, $stateParams, MapService, emergencies, emergency, base_stations){
  $scope.emergencies = emergencies.emergencies;
  $scope.emergency = emergency;
  $scope.MapService = MapService;
  $scope.buttons = { selected: "" };

  if(emergency.id)
  {
    console.log("Setting up pusher");
    try {
      $rootScope.pusher.disconnect();
      console.log("Disconnected Pusher.");
    }
    catch(err) {
    }
    $rootScope.pusher = new Pusher('2b3556cbc526febaf37e', { encrypted: true });
    $rootScope.channel = $rootScope.pusher.subscribe('emergency_' + emergency.id);
    $rootScope.channel.unbind();
    $rootScope.channel.bind('drone_location', function(data) {
      console.log("Pusher: Received location:", data);
      angular.forEach($scope.markers, function(marker) {
        if(marker.key + "" === data.drone_id + "")
        {
          marker.lat = parseFloat(data.lat);
          marker.lng = parseFloat(data.lng);
          console.log("Marker updated");
        }
      });
    });
    $rootScope.channel.bind('drone_battery', function(data) {
      console.log("Pusher: Received battery:", data);
    });
    $rootScope.channel.bind('photo', function(data) {
      console.log("Pusher: Received photo.");
      $scope.emergency.photos.push(data.url);
      $scope.$apply();
    });
  }

  $scope.addEmergency = function() {
    if($scope.buttons.selected === "cancel")
      $state.go('emergencies');
    else
    {
      // Compile services
      var services = new Array();
      for (var key in $scope.services_required) {
        services.push(key);
      }
      services = services.join(", ");

      emergencies.create({
        caller_name: $scope.caller_name,
        caller_number: $scope.caller_number,
        description: $scope.description,
        services_required: services,
        location: $scope.location,
        latitude: $scope.latitude,
        longitude: $scope.longitude,
        status: "pending"
      }).then(function(e){
        var id = e.data.id;
        if($scope.buttons.selected === "assign-drones")
        {
          console.log("Going to emergency's flight plan...");
          $location.path('/flightplan/new/' + id);
        }
        else
          $location.path('/emergencies/' + id);
      }, function(reason){
        alert("Failed: " + reason)
      });
    }
  };

  $scope.openEmergency = function(id) {
    $location.path('/emergencies/'+id);
  };
  $scope.setEmergencyStatus = function(emergency, status) {
    emergencies.setEmergencyStatus(emergency.id, status);
    emergency.status = status;
  };

  // Plotting on map
  $scope.markers = angular.copy(MapService.basesToMarkers(base_stations.bases));
  $scope.home_location = angular.copy($scope.markers[0]);

  // Add emergency marker if it exists
  if($scope.emergency.latitude)
  {
    var emergency_location = {lat: parseFloat(emergency.latitude), lng: parseFloat(emergency.longitude), zoom: 17, message: "Emergency's location", key: 'emergency_location', icon: MapService.icons.emergency};
    $scope.markers.push(emergency_location);
    $scope.home_location = angular.copy(emergency_location);
  }
  // Add drones to map if it exists
  if($scope.emergency.flights && $scope.emergency.flights.length > 0)
  {
    angular.forEach($scope.emergency.flights, function(flight){
      var flight_entries = flight.flight_entry_locations;
      // flight_entries.sort(function (a, b) { return a.created_at - b.created_at; });
      var lat, lon;
      if(flight_entries[0] !== undefined)
      {
        lat = flight_entries[0].latitude;
        lon = flight_entries[0].longitude;
      }
      else
      {
        lat = flight.drone.base_station.latitude;
        lon = flight.drone.base_station.longitude;
      }
      var d_loc = {lat: parseFloat(lat), lng: parseFloat(lon), zoom: 13, message: flight.drone.name, key: flight.drone.id, icon: MapService.iconFromStatus(flight.drone.status)};
      $scope.markers.push(d_loc);
    });
  }

  $scope.$on("leafletDirectiveMap.click", function(event, args) {
    var leafEvent = args.leafletEvent;
    if($state.current.name === "new_emergency") // Only if no emergency is assigned
    {
      // Remove previous location if assigned
      $scope.markers = $scope.markers.filter(function (o) { return o.key !== "emergency_location"; });
      // Add new location
      $scope.markers.push({
        lat: leafEvent.latlng.lat,
        lng: leafEvent.latlng.lng,
        message: "Emergency's location",
        key: 'emergency_location',
        icon: MapService.icons.emergency,
        draggable: true
      });
      $scope.latitude = leafEvent.latlng.lat;
      $scope.longitude = leafEvent.latlng.lng;
    }
  });

}]);