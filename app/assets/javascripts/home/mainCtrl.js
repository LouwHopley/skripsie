angular.module('droneDispatch')
.controller('MainCtrl', [
'$scope',
'emergencies',
function($scope, emergencies){
  $scope.emergencies = emergencies.emergencies;

  $scope.addPost = function(){
    if(!$scope.title || $scope.title === '') { return; }
    $scope.emergencies.push({title: $scope.title});
    $scope.title = '';
  };
}]);