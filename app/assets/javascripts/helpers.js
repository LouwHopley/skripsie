angular.module('droneDispatch')
.run(function($rootScope){
  $rootScope.contextualPriority = function(i) {
    var t = "default";
    switch(i){
      case 2:
        t = "info";
        break;
      case 3:
        t = "success";
        break;
      case 4:
        t = "warning";
        break;
      case 5:
        t = "danger";
        break;
      default:
        break;
    }
    return t;
  };
  $rootScope.contextualEmergencyStatus = function(status) {
    var t = 'default';
    switch(status){
      case 'pending':
        t = 'danger';
        break;
      case 'enroute':
        t = 'warning';
        break;
      case 'onscene':
        t = 'info';
        break;
      case 'complete':
        t = 'success';
        break;
      default:
        break;
    }
    return t;
  };
  $rootScope.contextualDroneStatus = function(status) {
    var t = 'default';
    switch(status){
      case 'inflight':
        t = 'info';
        break;
      case 'grounded':
        t = 'danger';
        break;
      case 'charging':
        t = 'warning';
        break;
      case 'ready':
        t = 'success';
        break;
      default:
        break;
    }
    return t;
  };
  $rootScope.console = console;
  $rootScope.alert = alert;
  $rootScope.parseFloat = parseFloat;
})
.filter('secondsToDateTime', [function() {
    return function(seconds) {
        return new Date(1970, 0, 1).setSeconds(seconds);
    };
}])
.filter('flightReady', [function() {
  return function(drones) {
    var filtered = [];
    angular.forEach(drones, function(drone) {
      if(drone.status === 'charging' || drone.status === 'ready') {
        filtered.push(drone);
      }
    });
    return filtered;
  };
}])
.filter('inRange', [function() {
  return function(drones, MapService, location) {
    var filtered = [];
    angular.forEach(drones, function(drone) {
      var avail_range = MapService.availableReturnRange(drone);
      var req_range = MapService.geoDistanceMeters(drone.location, location)/1000;
      if(req_range < avail_range)
        filtered.push(drone);
    });
    return filtered;
  };
}])
.directive('buttonId', function() {
  return {
    restrict: "A",
    link: function(scope, element, attributes) {
      element.bind("click", function(){ scope.buttons.selected = attributes.buttonId; });           
    }
  }
});;