angular.module('droneDispatch')
.factory('MapService', [
  function(){
    var o = {};

    // Define icons
    o.icons = {
      base : {iconUrl: 'marker_icons/helipad.png', iconSize: [32, 32], iconAnchor: [16, 16], popupAnchor: [0, 0]},
      camera : {iconUrl: 'marker_icons/camera.png', iconSize: [32, 32], iconAnchor: [16, 32], popupAnchor: [0, 0]},
      relay : {iconUrl: 'marker_icons/relay.png', iconSize: [32, 32], iconAnchor: [16, 32], popupAnchor: [0, 0]},
      drone : {iconUrl: 'marker_icons/drone.png', iconSize: [32, 32], iconAnchor: [16, 16], popupAnchor: [0, 0]},
      emergency : {iconUrl: 'marker_icons/emergency.png', iconSize: [32, 32], iconAnchor: [16, 32], popupAnchor: [0, 0]}
    };
    o.iconFromStatus = function(status){
      return o.icons.drone;
      switch(status)
      {
        case 'inflight':
          return o.icons.drone_info;
        case 'grounded':
          return o.icons.drone_danger;
        case 'charging':
          return o.icons.drone_warning;
        case 'ready':
          return o.icons.drone_success;
        default:
          return;
      }
    };

    o.basesToMarkers = function (bases) {
      var a = [];
      angular.forEach(bases, function (base, index) {
        var m = {
          lat: base.latitude,
          lng: base.longitude,
          zoom: 17,
          message: base.name,
          key: base.name.toLowerCase().replace(/\s/g, "_") + "_" + base.id,
          icon: o.icons.base
        };
        a.push(m);
      });
      return a;
    };
    o.dronesToMarkers = function (drones) {
      var a = [];
      angular.forEach(drones, function (drone, index) {
        var m = {
          lat: drone.latitude,
          lng: drone.longitude,
          zoom: 17,
          message: drone.name,
          key: drone.name.toLowerCase().replace(/\s/g, "_") + "_" + drone.id,
          icon: o.icons.drone
        };
        a.push(m);
      });
      return a;
    };
    o.geoDistanceMeters = function(loc1, loc2) {
      if(loc1.lat == null || loc2.lat == null)
        return -1;
      var lat1 = loc1.lat;
      var lon1 = loc1.lon;
      var lat2 = loc2.lat;
      var lon2 = loc2.lon;
      /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
      /*  Latitude/longitude spherical geodesy formulae & scripts           (c) Chris Veness 2002-2015  */
      /*   - www.movable-type.co.uk/scripts/latlong.html                                   MIT Licence  */
      /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
      Number.prototype.toRadians = function() { return this * Math.PI / 180; };
      var R = 6371000; // metres
      var φ1 = lat1.toRadians();
      var φ2 = lat2.toRadians();
      var Δφ = (lat2-lat1).toRadians();
      var Δλ = (lon2-lon1).toRadians();

      var a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
              Math.cos(φ1) * Math.cos(φ2) *
              Math.sin(Δλ/2) * Math.sin(Δλ/2);
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

      var d = R * c;
      return d;
    };
    o.availableReturnRange = function(drone) {
      return ((drone.battery_level / drone.power_usage) * drone.speed / 1000)/2;
    };
    o.availableReturnTime = function(drone) {
      return (drone.battery_level / drone.power_usage);
    };
    return o;
  }
]);