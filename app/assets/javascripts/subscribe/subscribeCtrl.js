angular.module('droneDispatch')
.controller('SubscribeCtrl', [
'$rootScope',
'$scope',
'$state',
'$location',
'$stateParams',
'MapService',
'emergencies',
'emergency',
function($rootScope, $scope, $state, $location, $stateParams, MapService, emergencies, emergency){
  $scope.MapService = MapService;
  $scope.emergency = emergency;

  $scope.subscribeToChannel = function(){
    var sc = $scope.shortcode;
    $location.path('/subscribe/' + sc);
  };

  if($scope.emergency.id)
  {
    console.log("Setting up pusher");
    try {
      $rootScope.pusher.disconnect();
      console.log("Disconnected Pusher.");
    }
    catch(err) {
    }
    $rootScope.pusher = new Pusher('2b3556cbc526febaf37e', { encrypted: true });
    console.log('emergency_' + $scope.emergency.id);
    $rootScope.channel = $rootScope.pusher.subscribe('emergency_' + $scope.emergency.id);
    $rootScope.channel.unbind();
    $rootScope.channel.bind('photo', function(data) {
      console.log("Pusher: Received photo.");
      if($scope.emergency.photos === undefined)
        $scope.emergency.photos = [];
      $scope.emergency.photos.push(data.url);
      $scope.$apply();
    });
  }

}]);