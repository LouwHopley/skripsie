angular.module('droneDispatch')
.controller('FlightPlanCtrl', [
'$scope',
'$state',
'$location',
'$stateParams',
'MapService',
'emergencies',
'emergency',
'drones',
'base_stations',
function($scope, $state, $location, $stateParams, MapService, emergencies, emergency, drones, base_stations){
  $scope.emergencies = emergencies.emergencies;
  $scope.emergency = emergency;
  $scope.drones = drones.drones;
  $scope.MapService = MapService;

  $scope.selectDrone = function(drone) {
    $scope.assigned_drone = drone;
  };

  $scope.createFlightPlan = function() {
    console.log("Started creating the flight plan");
    console.log("Objectives:", $scope.objectives);
    console.log("Emergency:", $scope.emergency)
    emergencies.fileFlightPlan($scope.emergency, $scope.assigned_drone, $scope.objectives);
    $location.path('/emergencies/' + $scope.emergency.id);
  };

  $scope.new_objective = {};
  $scope.objectives = [];
  $scope.addWaypoint = function() {
    if($scope.new_objective.action === undefined || $scope.new_objective.action === "")
    {
      alert("Please specify an action.");
      return;
    }
    if($scope.new_objective.action === "relay" && ($scope.new_objective.relay === undefined || $scope.new_objective.duration === undefined))
    {
      alert("Please specify the relay to be toggled and a duration in seconds.");
      return;
    }
    var action = "";
    var map_icon;
    if($scope.new_objective.action === "photo")
    {
      action = $scope.new_objective.action;
      map_icon = MapService.icons.camera;
    }
    if($scope.new_objective.action === "relay")
    {
      action = $scope.new_objective.action + "_" + $scope.new_objective.relay + "_" + $scope.new_objective.duration;
      map_icon = MapService.icons.relay;
    }
    console.log("Action:", action);

    $scope.objectives.push({action: action, lat: $scope.new_waypoint.lat, lng: $scope.new_waypoint.lng, key: (action + "_" + $scope.objectives.length) });

    // Remove previous temp marker
    $scope.markers = $scope.markers.filter(function (o) { return o.key !== "new_waypoint"; });

    // Add permanent marker
    $scope.markers.push({
      lat: $scope.new_waypoint.lat,
      lng: $scope.new_waypoint.lng,
      zoom: 17,
      message: $scope.objectives.length + ": " + $scope.new_objective.action,
      key: action + "_" + $scope.objectives.length,
      icon: map_icon
    });
    $scope.new_objective = {};
    $scope.new_waypoint = undefined;

  };

  // Plotting on map
  if($scope.emergency.latitude)
  {
    console.log("Plotting emergency");
    $scope.markers = angular.copy(MapService.basesToMarkers(base_stations.bases));
    $scope.home_location = angular.copy($scope.markers[0]);
    var emergency_location = {lat: parseFloat(emergency.latitude), lng: parseFloat(emergency.longitude), zoom: 17, message: "Emergency's location", key: 'emergency_location', icon: MapService.icons.emergency};
    $scope.markers.push(emergency_location);
    $scope.home_location = angular.copy(emergency_location);
  }

  $scope.$on("leafletDirectiveMap.click", function(event, args) {
    var leafEvent = args.leafletEvent;

    $scope.new_waypoint = {lat: leafEvent.latlng.lat, lng: leafEvent.latlng.lng};

    // Remove previous temp marker
    $scope.markers = $scope.markers.filter(function (o) { return o.key !== "new_waypoint"; });
    // Add temp marker
    $scope.markers.push({
      lat: leafEvent.latlng.lat,
      lng: leafEvent.latlng.lng,
      zoom: 17,
      message: "New Waypoint",
      key: 'new_waypoint',
      icon: MapService.icons.drone
    });

  });

}]);