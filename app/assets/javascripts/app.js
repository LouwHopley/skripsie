angular.module('droneDispatch', ['ui.router', 'templates', 'angular-carousel', 'leaflet-directive'])
.config([
'$stateProvider',
'$urlRouterProvider',
function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('home', {
      url: '/home',
      templateUrl: 'home/_home.html',
      controller: 'MainCtrl'
    })
    .state('emergencies', {
      url: '/emergencies',
      templateUrl: 'emergencies/_emergencies.html',
      controller: 'EmergenciesCtrl',
      resolve: {
        emergencyPromise: ['emergencies', function(emergencies) {
          return emergencies.getAll();
        }],
        baseStationsPromise: ['base_stations', function(base_stations) {
          return base_stations.getAll();
        }]
      }
    })
    .state('new_emergency_step2', {
      url: '/flightplan/new/{emergency_id}',
      templateUrl: 'flightplan/_new_flightplan.html',
      controller: 'FlightPlanCtrl',
      resolve: {
        dronesPromise: ['drones', function(drones) {
          return drones.getAll();
        }],
        emergency: ['$stateParams', 'emergencies', function($stateParams, emergencies) {
          return emergencies.get($stateParams.emergency_id);
        }],
        baseStationsPromise: ['base_stations', function(base_stations) {
          return base_stations.getAll();
        }]
      }
    })
    .state('new_emergency', {
      url: '/emergencies/new',
      templateUrl: 'emergencies/_new_emergency.html',
      controller: 'EmergenciesCtrl',
      resolve: {
        baseStationsPromise: ['base_stations', function(base_stations) {
          return base_stations.getAll();
        }]
      }
    })
    .state('emergency', {
      url: '/emergencies/{id}',
      templateUrl: 'emergencies/_emergency.html',
      controller: 'EmergenciesCtrl',
      resolve: {
        emergency: ['$stateParams', 'emergencies', function($stateParams, emergencies) {
          return emergencies.get($stateParams.id);
        }],
        baseStationsPromise: ['base_stations', function(base_stations) {
          return base_stations.getAll();
        }]
      }
    })
    .state('drones', {
      url: '/drones',
      templateUrl: 'drones/_drones.html',
      controller: 'DronesCtrl',
      resolve: {
        dronesPromise: ['drones', function(drones) {
          return drones.getAll();
        }],
        drone_view : function() {return false;}
      }
    })
    .state('drone', {
      url: '/drones/{id}',
      templateUrl: 'drones/_drone.html',
      controller: 'DronesCtrl',
      resolve: {
        drone: ['$stateParams', 'drones', function($stateParams, drones) {
          return drones.get($stateParams.id);
        }],
        drone_view : function() {return true;}
      }
    })
    .state('edit_drone', {
      url: '/drones/{id}/edit',
      templateUrl: 'drones/_edit_drone.html',
      controller: 'DronesCtrl',
      resolve: {
        drone: ['$stateParams', 'drones', function($stateParams, drones) {
          return drones.get($stateParams.id);
        }],
        drone_view : function() {return true;}
      }
    })
    // .state('new_drone', {
    //   url: '/drones/new',
    //   templateUrl: 'drones/_new_drone.html',
    //   controller: 'DronesCtrl'
    // })
    .state('subscribe', {
      url: '/subscribe',
      templateUrl: 'subscribe/_subscribe.html',
      controller: 'SubscribeCtrl',
    })
    .state('subscribed', {
      url: '/subscribe/{shortcode}',
      templateUrl: 'subscribe/_subscribed.html',
      controller: 'SubscribeCtrl',
      resolve: {
        emergency: ['$stateParams', 'emergencies', function($stateParams, emergencies) {
          return emergencies.getFromShortcode($stateParams.shortcode);
        }]
      }
    });
  $urlRouterProvider.otherwise('home');
}]);