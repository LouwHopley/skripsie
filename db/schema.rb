# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151011115358) do

  create_table "base_stations", force: true do |t|
    t.string   "name"
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "drones", force: true do |t|
    t.string   "name"
    t.float    "power_usage"
    t.float    "speed"
    t.string   "status"
    t.integer  "base_station_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "emergencies", force: true do |t|
    t.string   "shortcode"
    t.string   "status"
    t.string   "caller_name"
    t.string   "caller_number"
    t.string   "services_required"
    t.string   "description"
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "flight_entry_batteries", force: true do |t|
    t.float    "level"
    t.integer  "flight_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "flight_entry_locations", force: true do |t|
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "flight_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "flight_plan_entries", force: true do |t|
    t.integer  "priority"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "action"
    t.datetime "completed_at"
    t.integer  "flight_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "flights", force: true do |t|
    t.datetime "take_off_at"
    t.datetime "touch_down_at"
    t.integer  "drone_id"
    t.integer  "emergency_id"
    t.integer  "version"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
