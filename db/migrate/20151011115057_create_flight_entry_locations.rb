class CreateFlightEntryLocations < ActiveRecord::Migration
  def change
    create_table :flight_entry_locations do |t|
      t.float :latitude
      t.float :longitude
      t.integer :flight_id

      t.timestamps
    end
  end
end
