class CreateEmergencies < ActiveRecord::Migration
  def change
    create_table :emergencies do |t|
      t.string :shortcode
      t.string :status
      t.string :caller_name
      t.string :caller_number
      t.string :services_required
      t.string :description
      t.float :latitude
      t.float :longitude

      t.timestamps
    end
  end
end
