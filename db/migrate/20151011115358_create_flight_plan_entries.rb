class CreateFlightPlanEntries < ActiveRecord::Migration
  def change
    create_table :flight_plan_entries do |t|
      t.integer :priority
      t.float :latitude
      t.float :longitude
      t.string :action
      t.datetime :completed_at
      t.integer :flight_id

      t.timestamps
    end
  end
end
