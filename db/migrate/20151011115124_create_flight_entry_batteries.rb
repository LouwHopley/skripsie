class CreateFlightEntryBatteries < ActiveRecord::Migration
  def change
    create_table :flight_entry_batteries do |t|
      t.float :level
      t.integer :flight_id

      t.timestamps
    end
  end
end
