class CreateFlights < ActiveRecord::Migration
  def change
    create_table :flights do |t|
      t.datetime :take_off_at
      t.datetime :touch_down_at
      t.integer :drone_id
      t.integer :emergency_id
      t.integer :version

      t.timestamps
    end
  end
end
