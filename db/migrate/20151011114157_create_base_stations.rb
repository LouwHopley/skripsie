class CreateBaseStations < ActiveRecord::Migration
  def change
    create_table :base_stations do |t|
      t.string :name
      t.float :latitude
      t.float :longitude

      t.timestamps
    end
  end
end
