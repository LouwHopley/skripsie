class CreateDrones < ActiveRecord::Migration
  def change
    create_table :drones do |t|
      t.string :name
      t.float :battery_level
      t.float :power_usage
      t.float :speed
      t.string :status
      t.integer :base_station_id

      t.timestamps
    end
  end
end
