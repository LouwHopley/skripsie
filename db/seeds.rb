base_station = BaseStation.create( name: "Eendrag Station", latitude: -33.931053, longitude: 18.872656 )

drone1 = Drone.create( name: "Test Drone 1", power_usage: 0.1, speed: 15, status: "grounded", base_station: base_station )
drone2 = Drone.create( name: "Test Drone 2", power_usage: 0.1, speed: 15, status: "grounded", base_station: base_station )


emergency = Emergency.create(
  shortcode: "1234",
  status: "pending",
  caller_name: "Samuel L. Jackson",
  caller_number: "646 123 4567",
  services_required: "police, ambulance",
  description: "Some description here.",
  latitude: -33.9305956230615,
  longitude: 18.873993381857872
  )

flight = Flight.create( drone: drone1, emergency: emergency)
FlightEntryLocation.create( flight: flight, latitude: -33.931053, longitude: 18.872656 )
FlightEntryBattery.create( flight: flight, level: 76 )

FlightPlanEntry.create( flight: flight, priority: 0, latitude: -33.93146, longitude: 18.87343, action: "photo")
# FlightPlanEntry.create( flight: flight, priority: 0, latitude: -33.930989, longitude: 18.872927, action: "photo")
FlightPlanEntry.create( flight: flight, priority: 1, latitude: -33.931229, longitude: 18.873192, action: "photo")
FlightPlanEntry.create( flight: flight, priority: 2, latitude: -33.931406, longitude: 18.872944, action: "photo")

# Locations for testing on Eendrag Bun. Base is located in NW corner.
# -33.930989, 18.872927 (NE corner)
# -33.931229, 18.873192 (SE corner)
# -33.931406, 18.872944 (S corner)