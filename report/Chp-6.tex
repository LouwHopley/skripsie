\chapter{Software Testing and Evaluation}

\section{Device}
In this section various aspects of the hardware device were tested on a software level. The components were first tested separately and then they were combined and tested whilst working together.

\subsection{Handshake and Configuration}
The handshake between the device and the web server was tested for three scenarios: starting clean with no personality; starting with a personality already configured; and starting with no signal to establish connection for handshake. It succeeded in all three tests and behaved as designed. In the event that there was no connection, the device would sleep a short while and try again until it had successfully connected.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{personality1}
\caption{Device handshake with web API to retrieve its personality, starting without a personality.}
\label{personality1}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{personality2}
\caption{Device handshake with web API to retrieve its personality, starting with a personality already assigned.}
\label{personality2}
\end{figure}

\subsection{GPS}
The GPS was tested for incorrect output as well as for all the different types of output received by the Raspberry Pi. First the output of the GPS module was directly tested by printing the messages read by the Raspberry Pi to the console. These messages were visually inspected and confirmed to be correct. Confirming correctness of the read data allowed testing the rest of the GPS interface. All the components functioned as designed. The standard output of the GPS module can be seen below in figure \ref{gpsconsole}.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{gpsconsole}
\caption{EM-406a GPS raw console output in NMEA format.}
\label{gpsconsole}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{gpsconsole2}
\caption{Console output of location coordinates stripped from EM-406a GPS NMEA output strings.}
\label{gpsconsole2}
\end{figure}

As described in Section 5.1, the GPS sometimes lost signal when used indoors. This was handled correctly by the software. It resulted in a log message stating that no GPS signal is available until it connects and receives a valid message again. This was as expected. In the event that a valid message was received, it was decoded and stored correctly in the queue as seen in figure \ref{gpsconsole2}.

\subsection{Camera}
The camera was tested individually before integrating it into the entire system. It behaved as expected and integrated well with the Raspberry Pi. It was however found that binding to the camera more than once resulted in the program `freezing' until restarted. This issue was overcome by simply binding to the camera when the library was initiated instead of every time that the camera was used.

\subsection{Relay Pins}
The toggling of the GPIO pins on the Raspberry Pi was tested for functionality. The pins behaved as designed and had no noticeable issues when they were toggled.

\subsection{Battery Sensor}
The battery sensor was tested for multiple scenarios. Different voltages were applied as input to the sensor circuit by means of both bench power source and actual LiPo batteries. It was also tested for when no battery is connected to the system. It was found that there was an offset in the ADC's measured voltage that did not match up to the physically measured voltages (by means of multimeter). The results are shown below in table \ref{tablebattcalib}. This issue was corrected and resolved by calibrating the values in the software by means of a lookup table.

\begin{table}[h]
\centering
\begin{tabular}{c c c c c}
Pin & Voltage & $x/1023$ & Converted & Offset Ratio \\ \hline
1 & 3.22 & 984 & 3.174 & 1.0145 \\
2 & 3.25 & 848 & 2.735 & 1.1883 \\
3 & 3.28 & 811 & 2.613 & 1.2553 \\
4 & 3.27 & 800 & 2.577 & 1.2689 \\
5 & 3.27 & 700 & 2.252 & 1.4520 \\
6 & 3.26 & 711 & 2.297 & 1.4192 \\
7 & 3.24 & 703 & 2.268 & 1.4286 \\
8 & 3.28 & 697 & 2.248 & 1.4591 \\

\end{tabular}
\caption{Measured ADC voltage readings offset, actual voltages and calibration values to correct offset.}
\label{tablebattcalib}
\end{table}

After the calibration correction was applied, the sensor circuit was tested again and found to be working more accurately.

\subsection{Queue System}
All the sensors and activities on the device depends on the queue system to function properly, as it is the backbone that binds all the other components together. In order to test its functionality, all the components and modules ware tested individually. They were monitored to confirm that they interact correctly and reliably with the queue. After it was established that all components work on their own, they were combined, one-by-one, to work together as designed. The queue system worked efficiently and reliably for each individual component, as well as combined between all threads, integrating the system together as a whole.

\subsection{Uploading}
The uploading of data is the final frontier of the system where all the captured data is sent to the web API. This part of the system was tested by populating the queue with mock data and images to see how the system behaved. The data were uploaded to the web server and were visually confirmed to be correct. It was also tested in circumstances where the internet connection was temporarily lost. The system behaved as expected: The post-requests failed, and the system held back the data to try again after a few seconds. It attempted uploading the data until the connection was reestablished, after which it successfully did so.

\subsection{Performance}
During this testing period, no problems regarding the Raspberry Pi's performance were observed. The only performance issue that was observed was during the uploading of data that depended on the speed of the internet connection.

\section{Web Application Program Interface}
The web application program interface (API) was tested separately for handling and for responding correctly to requests. The online service Hurl.it was used to test the API with mock data. Requests on all the routes were simulated and confirmed to be working as designed. The API received the data and stored it correctly.

The integration with the Postgres database was tested and behaved correctly. Amazon's S3 for image storing was tested for uploading and retrieving images. The connection to S3 was extremely slow on random occasions, causing the whole system to slow down or sometimes `hang'. This affected the dispatch interface as well as the reporting app.

\section{Dispatch Interface}
The user interface of the dispatch system was tested by actual user testing. The interface was also tested by adding mock data to the web API's database to see if the interface interprets it correctly. Emergencies were correctly added as designed and the data were correctly sent to the database. This was confirmed by investigating the new entries to the database. The interface behaved correctly except for the map updates. It was found that the drone marker did not reposition itself on the map each time, even though the Pusher messages were correctly received. This issue seemed to be caused by the incorrect method for updating marker positions with Leaflet. In general the interface worked sufficiently, but could be improved and optimised.

\section{Mobile Monitoring Interface}
The requirements for the mobile monitoring interface was met. The system was tested by subscribing to different emergencies on the system. The emergencies displayed correctly.

\section{Reporting Mobile App}
The reporting app behaved as expected. The functionality of the app was tested and the web API database was monitored for correctness of uploaded data. The user's position was captured successfully and the data were found to be correct and as designed.