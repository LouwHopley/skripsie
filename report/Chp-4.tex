\chapter{Software Design}
\section{Overview}

In this design there are five key subsystems that integrate with one another. These five subsystems consist of the web application program interface (API), drone-attachable device, dispatch interface, mobile monitoring interface and the reporting mobile app. The main component, the web API, is in the middle, connecting the other four subsystems by means of a star topology. This is shown in figure \ref{subsystems} below. Each of these subsystems are closely integrated with one another and they are discussed in the following chapters.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{subsystems}
\caption{Subsystem communication integration diagram.}
\label{subsystems}
\end{figure}

\section{Device Overview}

The drone-attachable device is an integration of multiple smaller subsystems. For the purpose of the prototype and to make the final system as open for integration as possible, each component will be considered and designed to be modular. The Raspberry Pi supports multiple different languages. Python however came recommended by the Raspberry Pi Foundation \cite{72} and was therefore chosen for this design.

\subsection{Hypertext Transfer Protocol Interface}

In order to fulfil the requirements for chapter 1, a Hypertext Transfer Protocol (HTTP) interface is designed to allow the device to communicate with the web API. This is the foundation of communications on the World Wide Web and it allows our system reliable communication over the cellular connection. This device's design only makes use of HTTP's GET and POST request methods.

The HTTP interface only has three defined functions: one to do the handshake and configuration of the drone, one to download the latest flight plan and finally one to upload the queue and all the photos.

\subsection{Handshake and Configuration}
When the device starts up for the first time, it connects to the system that has no knowledge of this device. In this design, a handshake was designed to configure the drone, creating a `personality' on the first start up and to be able to identify it from there onwards. The system first checks if it has a personality stored locally, if it does not it requests a new one from the web API server. The personality includes the device's identification number as stored on the web API, as well as the device's base station number and coordinates and a random bird name.
A list of bird names were compiled and used to assign a random bird name to a newly configured device, in order to make it more user friendly for the operator to identify devices. This process is shown more clearly in figure \ref{handshake}.

\begin{figure}[h]
\centering
\includegraphics[scale=0.6]{handshake}
\caption{Device handshake and configuration with web API server.}
\label{handshake}
\end{figure}

\subsection{GPS Interface}
To fulfil the requirements described in chapter 1, a GPS module is used to get accurate real-time location updates. This allows the system to know when it arrives at predefined locations where it should execute instructed tasks. The GPS module is based on the National Marine Electronics Association (NMEA) standard and sends sentences at a baud rate of 4800 bits/second \cite{34}.

\begin{quote}
	\begin{quotation}
		The idea of NMEA is to send a line of data, called a sentence, that is totally self contained and independent from other sentences.
	\end{quotation}
	\begin{quotation}
		``Each sentence begins with a `\$' and ends with a carriage return/line feed sequence and can be no longer than 80 characters of visible text (plus the line terminators). The data is contained within this single line with data items separated by commas. The data itself is just ascii text and may extend over multiple sentences in certain specialized instances but is normally fully contained in one variable length sentence.'' \cite{34}
	\end{quotation}
\end{quote}

In this design a software module is created to interface with the GPS module, to process incoming characters from the GPS module and finally to strip coordinates out of the sentences. The important sentence messages that contains the module's latitude and longitude starts with either `\$GPGGA' or `\$GPRMC' as found in the EM-406a's datasheet \cite{35}. Both these strings contain the current coordinates of the module. See figure \ref{gpsoutput} below for a visual representation of this data and its components. When one of these two messages are fully received, it is decoded and split up into its components where the latitude and longitude components are retrieved. These location coordinates are then finally added to the queue which is described in more detail in section 4.2.8 below. The process of the GPS interface is shown in figure \ref{gpsprocess}.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{gpsoutput}
\caption{NMEA `\$GPGGA' and `\$GPRMC' message latitude and longitude components.}
\label{gpsoutput}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[scale=0.6]{gpsprocess}
\caption{GPS message processing sequence.}
\label{gpsprocess}
\end{figure}

\subsection{Battery Sensor Interface}
The requirements of the device state that it should be able to detect the number of LiPo cells connected and then measure their voltages. The reading of the actual cells are described in Chapter 3, Section 3.5. To interface the Raspberry Pi with the MCP3008 analog-to-digital converter (ADC), the Serial Peripheral Interface (SPI) bus protocol is used over the Raspberry Pi's general-purpose input/output (GPIO) pins. This allows the Python code to read the MCP3008's data output.

The MCP3008 outputs the voltage read between 0V to 3.3V as an integer value between 0 and 1023. This transformation from an analog voltage to a limited 10-bit voltage does create quantization, but it is negligible in this design application. To transform this value back to the actual voltage, a mapping equation as seen in equation \ref{eq3}, is used.

\begin{equation}
\label{eq3}
\begin{split}
V = \frac{V_{INPUT}}{1023} * 3.3
\end{split}
\end{equation}

In order to determine the voltage of an actual cell, this formula is first applied to all 8 channels from the ADC to determine the actual voltage read from each of the ADC's pins. Thereafter the voltages are scaled up relatively using their respective voltage division ratios created by the resistor voltage dividers. This allows the software to know the exact voltage output of the battery on the respective pin.

When the actual voltages of the battery pins are known, the voltage accumulation, due to the cells that are in series, is reversed to determine each cell's individual voltage. Cells with a zero-voltage (or very close to zero) are categorised as missing. Counting all the cells that are not missing, results in the total number of cells of the battery pack.

\subsection{Relay Pins Interface}
The relay pins interface with the Raspberry Pi by means of the general-purpose input/output (GPIO) pins on the board. These pins can be turned on or off to create a digital logic `high' or `low'. In order to toggle the transistor connected to these pins, the software module is created that allows the system to configure the pin as an output, to set it either `high' or `low', and also to unbind from and close the pin. Closing the pin would only be required when the program comes to a halt, which would in the current design of this embedded never be required as the system would simply be turned of at the end of use. This will allow the system integration to toggle the relays.

\subsection{Camera Interface}
Due to the use of the Raspberry Pi Camera Module, the software integration can be simplified by using a popular Python library called `picamera' \cite{61}. The camera will take pictures on request and store it in a given directory for the queue processor to handle it at a later stage.

In order to prevent the limited memory space on the SD card to fill up with pictures, images that are successfully uploaded to the web API will be removed from the device.

The drone will be commanded to enter a hovering position above its designated target in order for it to stabilize and minimize motion blur. The picture will also be taken from a higher altitude in order to prevent movement blur and to capture more of the emergency scene. This is discussed in more detail in section 4.2.8. Mounting and positioning of the camera module is described in section 3.4.

The camera has a horizontal viewing angle of about 53.50 degrees and a vertical viewing angle of about $41.41^{\circ}$ \cite{71}. This needs to be taken into account when determining the high from which the drone takes its pictures.

It is chosen arbitrarily to have a minimum side length coverage of 50 meters in the images in order to capture most of an emergency scene and to allow for slight location errors. If the shorter length of the image needs to be at least 50 meters, then the vertical viewing angle, being smaller, is the determining factor for the image height. This height is now calculated in equation \ref{eq_height} and shown graphically in figure \ref{triangle}.


\begin{figure}[h]
\centering
\includegraphics[scale=0.5]{triangle}
\caption{Raspberry Pi Camera Module viewing angle triangle with side $B$ indicating the ground plane and the top corner the camera.}
\label{triangle}
\end{figure}

\begin{equation}
\label{eq_height}
\begin{split}
\tan(\frac{b}{2}) &= \frac{\frac{B}{2}}{h} ; B = 50m, b = 41.41^{\circ} \\
\tan(\frac{41.41}{2}) &= \frac{\frac{50}{2}}{h} \\
h &= \frac{25}{\tan(20.705)} \\
  &= 66.14m \\
  &\approx 70m
\end{split}
\end{equation}

This shows that the drone should hover at a height of at least 70m from the ground.

\subsection{Drone Interface}
The specification requires the device to be open and extensible for integration to different types of drones. To provide this type of interface, a Python module that has the functions defined to execute the required tasks, is created separately. These tasks include take-off, fly-to, hover, and touch-down. It also includes a function that returns the drone's current status such as "grounded", "hovering", "in flight" or "crashed". This hardware device however does not perform object avoidance or navigation as it is not a flight controller. It is therefore required from the integrating designer to take these two factors into account.

This interface is integrated into the system but open on the one end for integration. The integration is discussed in more detail in the next section.

\subsection{System Integration Design}
A few key factors need to be taken into account to integrate all the components in the above sections into a working drone system. The drone needs to execute non-interrupting tasks, such as checking location for actions to perform, reading battery sensor data, checking the GPS and uploading data to the web API, without these tasks blocking each other's processes. Multithreading is used to achieve separation of these processes. Multithreading allows for better utilization of processor resources \cite{36} which is not a core design feature for this design, but it is intended to improve the system's overall performance. Near-parallelization is the main advantage of multithreading used in this design as it allows each thread to run independently from one another allowing them to continue executing when another thread blocks. This concept and design is shown in figure \ref{threads} below where the process flow of the whole system with multithreading is indicated.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{threads}
\caption{Complete system integration process flow diagram.}
\label{threads}
\end{figure}

The system is designed to perform an action, of either taking a picture or toggling a relay, when it is within a five meter radius from the next designated waypoint location. This distance calculation is made by a formula adapted from Chris Veness' Latitude/longitude spherical geodesy formulae \& scripts \cite{60}, that calculates a distance between two geographical coordinates by taking into account the curvature of the earth. It is shown in listing \ref{distance} below.


\begin{lstlisting}[caption={A Python implementation of Chris Veness' code to calculate the distance in meters between two geographical coordinates by taking the curvature of the earth into account.}, label=distance, language=Python]
  q1 = math.radians(lat1)
  q2 = math.radians(lat2)
  dq = math.radians(lat2-lat1)
  dl = math.radians(lon2-lon1)
  a = math.sin(dq/2) * math.sin(dq/2) + math.cos(q1)
     * math.cos(q2) * math.sin(dl/2) * math.sin(dl/2)
  c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
  d = 6371000 * c # 6371000 is the radius of earth
\end{lstlisting}


%\begin{figure}[h]
%\centering
%\includegraphics[scale=0.6]{distance}
%\caption{A Python implementation of Chris Veness' code to calculate the distance in meters between two geographical coordinates by taking the curvature of the earth into account.}
%\label{distance}
%\end{figure}

To accommodate situations where the internet connection might get lost due to connectivity issues in the uploader thread, it is designed to go to sleep and try again in a few seconds if the queue fails to upload. This will continue until the connection has been re-established.

\section{Web Application Program Interface}

To meet the specifications as set in chapter 1, a central web server running an API needs to be in place. Although multiple other options, such as NodeJS \cite{48} or Django \cite{70}, exists for creating a web framework, the decision was made to build the system using Ruby-on-Rails \cite{38} instead, as I was already somewhat familiar with the framework, allowing faster development of the prototype. The API is based on the Representational State Transfer (REST) architecture style, as it is widely supported and accepted \cite{37}.

The data structure is designed with the emergencies and drones as the two main components. Flight plans are the connection between the two. The full data structure is shown below in figure \ref{datastruct}.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{datastruct}
\caption{Web API system's data structure and relationships.}
\label{datastruct}
\end{figure}

Heroku, a cloud hosting service, provides a complete solution for hosting web systems \cite{39}. Their systems are preconfigured and ideal for hosting a prototype system. Heroku will be used to host this web API. For data storage, most database systems will suffice. PostgreSQL is used in this design for a database, as Heroku provides it as the default preconfigured database for an app \cite{40}. Because Heroku does not allow for the storage of uploaded files on their services \cite{41}, Amazon's Simple Storage Service (S3) will be used to store the newly uploaded images \cite{42}.

The API handles the dispatch system, the device connection, and the reporting app on three separate levels, allowing for complete separation of systems. The dispatch system and the mobile monitoring interface has full access to all the data using a RESTful approach. The device and the reporting app are limited to certain routes as indicated in figure \ref{routes} below.

\begin{lstlisting}[caption={Device and reporting app available API routes.}, label=routes, language=Ruby]
# Reporting App
get "/app/emergencies/create"
get "/app/emergencies/:number/:name/:id"
get "/app/emergencies/:number/:name"

# Hardware device upload
post "/device/:id/update"
post "/device/:id/upload"
get   "/device/:id/flightplan"
get "/device/subscribe"
\end{lstlisting}

\section{Dispatch Interface}
In order for the dispatch operator to efficiently manage emergencies and drones, a minimalistic approach is used to create a dashboard that allows the operator to view emergencies in their current state, create new emergencies and manage the emergencies in progress. The dashboard allows the operator to assign a flight plan for a drone to dispatch it to the scene. When a new emergency is created, a randomized short code is created that allows a first responder to subscribe to the emergency on their mobile monitoring interface.

For the graphical user interface (GUI), multiple different approaches exist. Due to it being web based and running in a web browser, it is required to use HyperText Markup Language (HTML), Cascading Style Sheets (CSS) and JavaScript \cite{43}. This could either be implemented directly, or frameworks built on top of these technologies could be used. The requirements state that ``Off-the-shelf modules and frameworks should be used where possible.'', indicating that a framework should be used instead.

Available frameworks for simplifying styling includes Twitter Bootstrap \cite{44}, Zurb Foundation \cite{45} and Pure.css \cite{46}. Twitter Bootstrap was chosen in this design due to its popularity, robustness and available support resources.

A few options and frameworks exist to dynamically render views. One option is to render the content server-side with technologies like Ruby-on-Rails \cite{38}, PHP \cite{47}, or NodeJS \cite{48}. The other option is to use a client based framework that extends HTML, CSS and JavaScript and runs in the browser. This second option requires another data source such as a RESTful API service. Because the system design already contains a RESTful API, a client based framework is chosen. Client based frameworks are also more responsive as all the source code is already loaded in the browser's memory. Framework options include Knockout.js \cite{49}, AngularJS \cite{50} and BackboneJS \cite{51}. AngularJS was chosen for this design. It is developed and maintained by Google, has detailed resources and documentation available and is extensible - countless open source extensions are available.

The dispatch interface is built using Twitter's Bootstrap as styling template and AngularJS as the framework that handles all the view and data processing. The user flow of the emergency component of the interface, is shown in figure \ref{dispatch}. Mapping for creating emergencies, creating flight plans and for viewing the status of emergencies is implemented by using LeafletJS \cite{56}, an interactive mapping library with the map data from OpenStreetMaps \cite{57}.

The graphical user interface (GUI) of the interface's view-emergency component can be seen below in figure \ref{dispatch4}. More components of the GUI can be seen in Appendix G.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{dispatch}
\caption{Operator usage flow of emergency component of dispatch interface.}
\label{dispatch}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=0.7\textwidth]{dispatch4}
\caption{Drone dispatch web-based interface: Viewing all active emergencies.}
\label{dispatch4}
\end{figure}

\section{Mobile Monitoring Interface}

The mobile monitoring interface is an extension to the dispatch interface. It is designed and built as an extension to the dispatch interface, and is thus also developed in AngularJS. The mobile monitoring part however is limited in functionality and only allows the user to subscribe-to and view emergencies, as per the specifications. The interface only exists of a subscribe-to-emergency screen and a view-emergency screen. The view emergency screen gives the user information regarding  the emergency and allows the user to scroll through all the drone-device uploaded photos.
The emergency viewing screen GUI of the mobile monitoring interface can be observed in figure \ref{mobileinterface} below.

\begin{figure}[h]
\centering
\includegraphics[width=0.7\textwidth]{mobileinterface}
\caption{Mobile emergency monitoring interface optimized for tablets.}
\label{mobileinterface}
\end{figure}

\section{Reporting Mobile App Interface}
The reporting mobile app is designed to allow a user to report an emergency quickly. When a user reports an emergency, the application is designed to capture the user's location and submit it, together with the user's name and phone number, to the web API. When a user launches for the first time, they are required to fill in and complete their personal information which includes their name and phone number. This allows the user to report an emergency quickly. The main screen, account details screen and new emergency screen can be seen in figure \ref{app}.

The reporting app can either be developed natively or as a hybrid app. Hybrid was chosen as it is similar in design to the other interfaces in this design. Ionic \cite{52} is an advanced hybrid app mobile app framework that utilises mobile-optimized HTML, CSS and Javascript components to allow building interactive applications \cite{53}. It is based on Cordova \cite{54}, a platform that allows development of native mobile apps using web technologies. Ionic is chosen as the preferred framework as it utilises AngularJS that is also used in other parts of this design.


\begin{figure}[h]
\centering
\includegraphics[scale=0.6]{app}
\caption{Reporting mobile app screen designs for main screen, account details screen and new emergency screen.}
\label{app}
\end{figure}

\section{Real-time Updates}

In order for the whole system to work seamlessly and to keep the users up to date in real-time, the service Pusher is used \cite{55}. Pusher is a web-socket service that handles all the heavy lifting of setting up sockets between implementations of systems and allows the developer to subscribe to and broadcast messages on channels.

The Pusher channel definitions are defined as shown in figure \ref{pusher} below. This is implemented on both the web API, where update messages are broadcast from, and on the dispatch interface and mobile monitoring interface where these updates are displayed. These updates include new photos that are uploaded by a device, location updates, and also battery level updates.

\begin{figure}[h]
\centering
\includegraphics[scale=0.5]{pusher}
\caption{Pusher message transport diagram indicating the message structures and how delivery to multiple clients work.}
\label{pusher}
\end{figure}
