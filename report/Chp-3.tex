\chapter{Hardware Design}
\section{Overview}

In order to design a hardware device  as described in chapter 1, it has to be broken down into distinct subsystems. The functionality and design of each of these subsystems will be described in the following sections. The hardware subsystems integration is shown in figure \ref{hardwareschem}. This figure is focused on the connection and communication between the hardware components. The design choices will be made with the weight of the device in mind, keeping it light within practical means for a proof-of-concept.

Although payload limitations vary per drone model, a baseline weight limit is required for this design. A brief interview with Willem Carel de Jongh, a Stellenbosch racing drone enthusiast, revealed that any payload with a weight below 200 grams, would be bearable by most drones. Therefore, 200 grams will be used as a maximum weight limit for this design.

\begin{figure}[h]
\centering
\includegraphics[scale=0.5]{hardwareschem}
\caption{Schematic of drone-attachable device hardware system.}
\label{hardwareschem}
\end{figure}

\section{Raspberry Pi}
The whole onboard system is controlled and integrated by a central processing unit. The Raspberry Pi is chosen as the device's main computational unit. Other options such as the BeagleBone Black \cite{64} or Intel Galileo \cite{65} exist but were disregarded as a Raspberry Pi fulfills the requirements and was already available prior to the design of this system. The Raspberry Pi is a complete microcomputer capable of integrating and processing all the required hardware components \cite{16}. Power supply for the Raspberry Pi will be described in the next section. Besides power, the only other component that needs to function, is onboard storage. Storage is provided in the form of a Secure Digital card (SD-Card) \cite{23}.

\section{Power Supply and Distribution}
In order to power the device, two options exist. Either power it with its own stable battery, or utilize the already connected drone battery. A separate battery would allow a more stable power source. The drone's battery experiences irregular voltage fluctuations due to the motors constantly changing speed and drawing high currents. The decision is made to use the drone's battery, despite the irregular fluctuations of the voltage, in order to minimize the weight of the device, by not carrying an extra battery. This design is simplified by having the battery already connected for measuring purposes. To ensure a reliable and stable regulated voltage supply for the system, a voltage regulation circuit is designed and implemented to create a stable 5V voltage supply as in figure \ref{voltagereg} below. The LM7805 voltage regulator \cite{24} is used in this design as it accepts an input voltage anywhere in the range of 7V to 20V and outputs stay stable at 5V. It is also rated up to 15W power output. The Raspberry Pi 2 Model B requires 1.8A at 5V, which means 9W in total \cite{25}.

\begin{figure}[h]
\centering
\includegraphics[scale=0.8]{voltagereg}
\caption{5V voltage regulation circuit to regulate and stabilize drone-battery input.}
\label{voltagereg}
\end{figure}

Voltage distribution to other components are directly from the Raspberry Pi's on board 5V and 3.3V output pins, as indicated in their respective sections below, except the relays are powered directly from the regulated 5V.

\section{Onboard Camera}

In order to fulfil the requirements as set out in chapter 1, the device needs to be able to take pictures at designated locations. The Raspberry Pi's standard Camera Module was chosen to achieve this. The Pi NoIR camera is used specifically as it has no infrared filter. This allows the camera to operate in much lower light settings and is able to perform better during more hours of the day. A representation of the camera module is shown in figure \ref{picam}.

In order to keep this design simplistic and easy to replicate, the camera would be mounted fixed in a downwards facing position. The Pi Camera only supports digital zooming which will not be used as it degrades the quality of the image. More on the process of taking images are described in section 4.2.6.


\begin{figure}[h]
\centering
\includegraphics[scale=0.6]{picam}
\caption{Raspberry Pi's NoIR camera module. Source: \cite{26}}
\label{picam}
\end{figure}

\section{Battery Sensor}
Drones mostly use lithium-ion polymer (LiPo) batteries as they are lighter, rechargeable and a very effective power source. These batteries consist of 3.8V cells combined in series to create a higher voltage level. These cells have a maximum voltage level of 4.2V. \cite{27} Smaller drones typically operate on 3S (3-cells) or 4S (4-cells) whereas bigger devices operate on sizes up to 8S (8-cells). These cells generally have two power outputs: One with the total combined voltage capable of delivering extremely high currents, and another connected to each cell, in order to measure the different voltages. An example of a 4S LiPo battery with its two different connection ports is shown in figure \ref{battery}.

\begin{figure}[h]
\centering
\includegraphics[scale=0.4]{battery}
\caption{Example 4S (4-cell) LiPo battery with both high-current and balancing connectors visible. Source: \cite{28}}
\label{battery}
\end{figure}

Designing a generic system that is capable of reading the voltage level of a drone, it is essential to automatically determine the amount of cells in the battery. This is achieved by always measuring the input of 8 cells. If there is only 6 cells, then the final reading of the two would be zero, indicating to the system that there is only 6 cells connected.

Due to the cell voltages accumulating as indicated in table \ref{cells} below, a method needs to be applied to determine the cells' individual voltage. This could either be done by means of hardware - a voltage subtracting circuit - or by means of software. In this design, it will be achieved by reading the pin's voltage and subtracting the previous voltages in software.

\begin{table}[h]
\centering
\begin{tabular}{c c c}
Cell Number & Cell Voltage & Pin Voltage (accumulated) \\ \hline
Cell 1 & 3.85 V & 3.85 V \\
Cell 2 & 3.90 V & 7.75 V \\
Cell 3 & 3.87 V & 11.62 V \\
Cell 4 & 3.89 V & 15.51 V
\end{tabular}
\caption{Example 4S (4-cell) battery cells voltage and accumulated pin voltage output.}
\label{cells}
\end{table}

The MCP3008 10bit 8-channel analog-to-digital converter will be used to convert the voltage level to a digital signal for the Raspberry Pi. This device is powered by the Raspberry Pi's 3.3V power output. The MCP3008 is limited to reading voltages between $V_{SS}$ and $V_{CC}$, or 0V and 3.3V in this case \cite{29}. In order to read up to the maximum voltages per cell, voltage division as in equation \ref{eq1} will be applied to the accumulated voltages as if the cells were fully charged to 4.2V each. Other options such as using op-amps circuits exist and could result in more accurate and reliable results, however it is considered overdesign for this feature of the system.

\begin{equation}
\label{eq1}
\begin{split}
V_{OUTPUT} = \frac{V_{INPUT}}{R_1 + R_2} * R_2
\end{split}
\end{equation}

\begin{figure}[h]
\centering
\includegraphics[scale=0.6]{voltagedivider}
\caption{Resistor voltage divider circuit.}
\label{voltagedivider}
\end{figure}

\begin{table}[h]
\centering
\begin{tabular}{c c c c c}
Cell Number & Pin Voltage & Ratio & Resistor A & Resistor B \\ \hline
Cell 1 & 4.2 V & 1.27:1 & 30k & 100k \\
Cell 2 & 8.4 V & 2.55:1 & 160k & 100k \\
Cell 3 & 12.6 V & 3.82:1 & 290k & 100k \\
Cell 4 & 16.8 V & 5.09:1 & 420k & 100k \\
Cell 5 & 21.0 V & 6.36:1 & 550k & 100k \\
Cell 6 & 25.2 V & 7.64:1 & 680k & 100k \\
Cell 7 & 29.4 V & 8.91:1 & 810k & 100k \\
Cell 8 & 33.6 V & 10.18:1 & 940k & 100k
\end{tabular}
\caption{Voltage division of 4.2C cells to bring all outputs down to below 3.3V with corresponding resistor values to achieve such division. Resistor A and B is connected in series in the order [input - A - output - B - ground]. Standard resistor values are chosen resulting in a minor divider offset.}
\label{cells4s}
\end{table}

Standard resistor values are chosen, resulting in consistent but inaccurate divisions. This error will be corrected by calibration in the software. Higher order resistor values were chosen in order to limit current draw from the battery.

Connection to the MCP3008 is accomplished by using a standard SPI-compatible serial interface \cite{29}. The designed circuit schematic of the voltage divider circuit with the MCP3008 analog-to-digital converter connected the Raspberry Pi, is shown in figure \ref{adcpi}.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{adcpi}
\caption{Voltage divider circuit for up to 8S (8-cells), LiPo battery sensor with MCP3008 and Raspberry Pi second generation.}
\label{adcpi}
\end{figure}


\section{Relay Pins}

For the device to be open and to allow users to add their own hardware, as per the requirements in chapter 1, switching pins are added. These pins will be used to toggle open or close at given locations allowing external components to perform actions. Input-output (IO) pins on the Raspberry Pi are used to toggle the relays.

The Raspberry Pi GPIO pins can only supply a limited amount of current before damage is induced. The GPIO pins source their power from the 3.3V source which can only provide a maximum current of 50mA. It is also suggested by Mosaic Industries not to exceed drawing more than 16mA per GPIO pin \cite{32}.

In order to not damage the Raspberry Pi when toggling a relay with a GPIO pin, a transistor is used to source current directly from the regulated 5V power source. A 2N2222A NPN switching transistor \cite{58} will be used for this application with a base resistor calculated to put the transistor in saturation mode when the GPIO is turned on to 3.3V. JRC-23FHS 5V relays will be used which requires a voltage of 5V over the coil terminals to switch states \cite{62}. This specific relay was chosen because of being available at the Stellenbosch University Electronic Lab.

First the relay's operating current was calculated because it was not given on the datasheet. The rated coil voltage ($V_C$) is specified as 5V and the coil resistance ($R_C$) as $166.7\Omega$. The minimum coil current ($I_C$) is calculated below:

\begin{equation}
\begin{split}
I_C &= \frac{V_C}{R_C} \\
    &= \frac{5}{166.7} \\
    &= 0.030A \\
    &= 30mA
\end{split}
\end{equation}

Using the minimum coil current of the relay ($I_C$)as the collector current of the transistor, the minimum required base current of the transistor can now be calculated. $hfe$ is read from the datasheet as 200.

\begin{equation}
\begin{split}
I_B &\simeq \frac{I_C}{hfe} \\
    &\simeq \frac{30m}{200} \\
    &\simeq 0.15mA \\
    &\simeq 150\mu A
\end{split}
\end{equation}

In order to ensure that the transistor is in saturation mode, a large base current is required. According to the 2N2222a transistor's datasheet, $V_{CE-sat}$ is $100mV$ and $V_{BE-sat}$ is $800mV$. A base resistor value ($R_1$) is arbitrarily chosen as $1K\Omega$. To confirm that this value will saturate the transistor and the the base current is large enough to toggle the relay, the following calculation was made:

\begin{equation}
\begin{split}
I_B &= \frac{ V_C-V_{BE-sat} }{R_1} \\
    &= \frac{3.3-0.8}{1000} \\
    &= 2.5mA >> 150\mu A
\end{split}
\end{equation}

To evaluate the correctness of the calculations above, Simulation Program with Integrated Circuit Emphasis (SPICE) was used. A SPICE circuit, representing the transistor-relay configuration, as seen in figure \ref{relay}, was created and simulated with NGSPICE \cite{ngspice}. The simulation results, seen in figure \ref{spice1}, reveal that the design calculations were indeed correct. SPICE revealed that the circuit only draws a few pico-Amperes when the base voltage is pulled `low' to $0V$. This `off-state' and the SPICE code used can be seen in Appendix H.

\begin{figure}[h]
\centering
\includegraphics[scale=0.5]{spice1}
\caption{Transistor-relay circuit SPICE results for when the transistor base voltage is a digital `high' of $3.3V$. With $i(Vc)$, $i(Vb)$ and $i(Ve)$, respectively representing the collector, base and emitter currents of the 2N2222a transistor.}
\label{spice1}
\end{figure}

Finally, a diode $D_1$ is added in reverse across the coil of the relay to protect the transistor from possible brief high voltages, produced when the relay is switched off. This circuit can be seen in figure \ref{relay} below. It is replicated three times to create three switchable pins on the device.

\begin{figure}[h]
\centering
\includegraphics[width=6cm]{relay}
\caption{Raspberry Pi GPIO compatible relay toggle with transistor circuit.}
\label{relay}
\end{figure}

\section{Global Positioning System}
In order for the device to fulfil the requirement of having accurate location data available, a Global Positioning System (GPS) unit is needed. The EM-406a serial GPS module (shown in figure \ref{gps}) was chosen due to being available prior to the design process and fulfilling the requirements. Multiple other similar modules, including the follow up EM-506a, exist and could also have been used. Connection between the module and the microcomputer is by means of serial communication and is therefore compatible with the Raspberry Pi. This module's communication protocol is NMEA (National Marine Electronics Association) which is the standard in GPS device output, and it happens over serial at a baud rate of 4800 bits/second.
The design choice is made to keep the GPS module separate from the device, connecting it with a wire. This allows the drone developer to mount the GPS module separately on the drone, to allow for a clear view of the sky and to minimize interference.

\begin{figure}[h]
\centering
\includegraphics[scale=1]{gps}
\caption{EM-406a GPS module. Source: \cite{30}}
\label{gps}
\end{figure}

The GPS module is powered by a 5V source level \cite{31}, but the output voltage of the transmitting terminal (TX) is 2.88V, which is above the 2.15V minimum limit for an input high voltage level required by the Raspberry Pi's GPIO pins \cite{32}. The circuit and the connection to the Raspberry Pi can be seen in figure \ref{gpscircuit} below.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{gpscircuit}
\caption{EM-406a to Raspberry Pi connection circuit with voltage step-down by means of voltage division.}
\label{gpscircuit}
\end{figure}

\section{Cellular Connection}
For the device to operate and upload data remotely, a more long-distance connection interface is required. The general availability of cellular makes this the ideal option for a data connection. The Raspberry Pi supports most USB 3G-devices. The Huawei E3533 (see figure \ref{3g}) was chosen as it was stocked by PiFactory \cite{63}, the same store the Raspberry Pi was purchased, as a reliable cellular module compatible with the Raspberry Pi. This module allows the Raspberry Pi to connect to the web API through the internet.

\begin{figure}[h]
\centering
\includegraphics[scale=0.6]{3g}
\caption{Huawei E3533 USB 3G dongle. Source: \cite{33}}
\label{3g}
\end{figure}

\section{Printed Circuit Board}

In order to make the device more compact to fulfil the requirements of being attached to a drone, the electronics need to be combined into a more compact design. This is achieved by designing a printed circuit board that can easily attach to the Raspberry Pi. The design can be seen in figure \ref{pcb}.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{pcb}
\caption{Printed Circuit Board design with EaglePCB.}
\label{pcb}
\end{figure}

\section{Casing}

For this proof-of-concept prototype a plastic casing is designed, using computer aided design (CAD) to enclose the device and to protect it from weather elements such as strong wind forces, as well as mist or dust. The casing is designed to mainly enclose the circuitry leaving open the USB ports for easy access and extensibility. The design will be 3D printed using Polylactic acid (PLA) plastic as it is readily available in this situation. PLA plastic will work sufficiently for a proof of concept, but Acrylonitrile-Butadiene-Styrene (ABS) plastic will provide a less brittle, more durable solution. The CAD designs are shown below in figures \ref{3d1} and \ref{3d2}.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{casebottom}
\caption{3D designed drone-attachable device enclosure bottom part.}
\label{3d1}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{casetop}
\caption{3D designed drone-attachable device enclosure top part.}
\label{3d2}
\end{figure}