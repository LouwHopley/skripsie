Rails.application.routes.draw do

  root to: "home#index"

  #Operator
  get "/operator" => "home#operator"

  #Subscriber
  get "/subscriber" => "home#subscriber"

  #API
  resources :emergencies do
    member do
      put '/update/:status' => 'emergencies#status_update'
      get '/photos' => 'emergencies#photos'
    end
    member do
      post '/flightplan/file' => 'emergencies#file'
      # resources :flights do
      #   member do
      #     resources :flight_entry_locations
      #     resources :flight_entry_batteries
      #     resources :flight_plan_entries
      #   end
      # end
    end
  end
  get '/emergencies/shortcode/:shortcode' => 'emergencies#show_from_shortcode'
  resources :drones do
    member do
      resources :flights do
        member do
          resources :flight_entry_locations
          resources :flight_entry_batteries
          resources :flight_plan_entries
        end
      end
    end
  end
  resources :base_stations

  # Reporting App
  get "/app/emergencies/create" => "app#create"
  get "/app/emergencies/:number/:name/:id" => "app#show"
  get "/app/emergencies/:number/:name" => "app#all"

  # Hardware device upload
  post "/device/:id/update" => "device#update"
  post "/device/:id/upload" => "device#upload"
  get   "/device/:id/flightplan" => "device#flightplan"
  get "/device/subscribe" => "device#subscribe"
end