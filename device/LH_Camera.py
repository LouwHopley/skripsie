import picamera
import time

camera = picamera.PiCamera()

def takePhoto():
  """ This takes a photo with the camera and stores it in returned `path`. """
  path = "photos/photo_" + str(time.time()) + ".jpg"
  # camera = picamera.PiCamera()
  camera.start_preview()
  print "CAMERA: Warming up."
  time.sleep(2)
  camera.capture(path)
  time.sleep(2)
  camera.stop_preview()
  print "CAMERA: Done."

  return path
