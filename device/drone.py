# Copyright 2015 Louw Hopley

# Multi threading
# https://www.raspberrypi.org/forums/viewtopic.php?t=72927&p=525356

import LH_HTTP
import LH_Camera
import LH_IO_Pins
import LH_Battery_Sensor
import LH_GPS
import LH_Drone_Interface

import json
import os.path
import time
import threading
import Queue

print "Started up."

# Some global operational variables
personality = {}
flightplan = {}
current_location = {"lat": 0, "lon": 0}
q = Queue.Queue()

# Personality check
personality = LH_HTTP.handshake();

def action_performer(_q, _fp, _loc, _p):
  """
  Performs actions defined in flightplan when
  designated location has been reached.
  """
  # print "DEBUG action_performer 1"
  # print "Action performer",_fp, _loc
  index = 0
  for entry in _fp:
    # print "DEBUG action_performer 2"
    # Find first priority item.
    # Break after that single entry is checked.
    if entry["completed_at"] == None:
      # print "DEBUG action_performer 3"
      # print "Entry:",entry
      distance = LH_GPS.metersBetweenPoints(_loc, {"lat": entry["latitude"], "lon": entry["longitude"]})
      print "Distance to next objective:", distance, entry["priority"]
      radius = 5
      if distance <= radius:
        print "Oooh, within reach of an objective."
        # Within 5m range. Something needs to be done.
        LH_Drone_Interface.hover(70 * 3.28084) # 70m to feet altitude
        time.sleep(1)
        # If entry is of type photo, take a picture
        if entry["action"] == "photo":
          print "Taking photo"
          path = LH_Camera.takePhoto()
          _q.put({ "activity": "photo", "path": path, "created_at": time.strftime("%Y-%m-%d %H:%M:%S"), "flight_plan_entry_id": entry["id"] })
          print "Picture taken: " + path

        # If entry is of type relay, toggle relay, wait for specified duration
        # and then toggle it off again.
        if entry["action"][:5] == "relay":
          relay_number = int(float(entry["action"][6:7])) - 1
          duration = int(float(entry["action"][8:]))
          pin = [11,11,11][relay_number]
          print "Turning on relay", relay_number, "on pin", pin, "for", duration, "seconds."
          _q.put({ "activity": "relay_on", "relay": relay_number, "created_at": time.strftime("%Y-%m-%d %H:%M:%S"), "flight_plan_entry_id": entry["id"] })
          LH_IO_Pins.write(pin, 1)
          print "Going into pin sleep"
          time.sleep(duration)
          print "Woke up from pin sleep"
          LH_IO_Pins.write(pin, 0)
          _q.put({ "activity": "relay_off", "relay": relay_number, "created_at": time.strftime("%Y-%m-%d %H:%M:%S"), "flight_plan_entry_id": entry["id"] })

        # Now that the action is complete, figure out what to do next.
        print "Done with action. Navigating to next waypoint."

        # If this is the last entry return home, else go to next location
        if index + 1 == len(_fp):
          print "Last flight plan entry. Returning to home location."
          next_lat = _p["base_station"]["latitude"]
          next_lon = _p["base_station"]["longitude"]
          LH_Drone_Interface.goTo(next_lat, next_lon, 70 * 3.28084) # 70m to feet altitude
        else:
          print "Flight plan entry remaining, going to it now."
          next_entry = _fp[index + 1]
          next_lat = next_entry['latitude']
          next_lon = next_entry['longitude']
          LH_Drone_Interface.goTo(next_lat, next_lon, 70 * 3.28084) # 70m to feet altitude

        # Update local entry that objective is complete. Also notify server
        entry["completed_at"] = time.strftime("%Y-%m-%d %H:%M:%S")
        _q.put({ "activity": "action_completed", "created_at": time.strftime("%Y-%m-%d %H:%M:%S"), "flight_plan_entry_id": entry["id"] })
        print "Added completed activity to queue."
      break
    index += 1

def thread_uploader(_q, _personality):
  """
  Check if anything is in the queue and process it.
  """
  while True:
    # print "THREAD Uploader"
    try:
      LH_HTTP.processQueue(_q, _personality)
      time.sleep(2)
    except:
      print "ERROR: Something went wrong in thread_uploader"

def thread_sensors(_q):
  """
  Read all sensors thread.
  """
  while True:
    try:
      # print "THREAD Sensors"
      # Read the battery voltages
      total_voltage, percentage, cells = LH_Battery_Sensor.ReadBatteryVoltages()
      _q.put({ "activity": "battery",
              "total_voltage": total_voltage,
              "percentage": percentage,
              "cells": cells,
              "created_at": time.strftime("%Y-%m-%d %H:%M:%S") })
      print "Sensors read"
      time.sleep(1)
    except:
      print "ERROR: Something went wrong in thread_sensors"

def thread_gps(_q, _loc):
  """
  Start up the GPS in his own thread.
  """
  LH_GPS.start(_q, _loc)

def thread_actions(_q, _fp, _loc, _p):
  """
  Check location/actions thread.
  """
  while True:
    try:
      # print "Thread Actions:",_fp
      action_performer(_q, _fp, _loc, _p)
      time.sleep(0.5)
    except Exception as e:
      print "ERROR: Something went wrong in thread_actions:", e

# Testing

# First get flightplan
print "Sorting out flight plan."
while True:
  flightplan = LH_HTTP.downloadFlightplan(flightplan, personality)
  if "grounded" in flightplan:
    print "Still grounded."
    time.sleep(10)
  else:
    print "Flight plan received."
    break
print "Rearranging priorities", flightplan
flightplan.sort(key=lambda priority: priority, reverse=False)
print "Done arranging",flightplan

print "Setting pins as output."
LH_IO_Pins.output(11)
# LH_IO_Pins.output(11)
# LH_IO_Pins.output(11)



# Start seperate threads
print "Starting up threads."

t4 = threading.Thread(target=thread_gps, args=(q, current_location))
t4.daemon = True
t4.start()

t3 = threading.Thread(target=thread_actions, args=(q, flightplan, current_location, personality))
t3.daemon = True
t3.start()

t2 = threading.Thread(target=thread_uploader, args=(q, personality))
t2.daemon = True
t2.start()

# t1 = threading.Thread(target=thread_sensors, args=(q,))
# t1.daemon = True
# t1.start()

# print "STEP 1: Take photo"
# action_performer(q)
# print "STEP 2: Process Queue"
# LH_HTTP.processQueue(q, personality)
# print "COMPLETED"

# Don't die!
try:
  while True:
    time.sleep(10)
except KeyboardInterrupt:
  print "\nDrone aborted. Good bye."



