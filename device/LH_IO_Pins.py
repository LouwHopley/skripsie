import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)

def output(pin):
  """ Configures the pin as an output. """
  GPIO.setup(pin, GPIO.OUT)

def write(pin, state):
  """ Writes the current state to the pin. """
  GPIO.output(pin, state)

def cleanup():
  """ Performs standard GPIO cleanup. """
  GPIO.cleanup()

# pin number if using BOARD, not GPIO number. http://raspberrypi.stackexchange.com/a/12967


if __name__ == '__main__':
  print "Testing Pin 11 LED"
  led = 11
  output(led)
  try:
    print("Starting...")
    while True:
      write(led, True)
      print("On")
      time.sleep(0.1)
      write(led, False)
      print("Off")
      time.sleep(0.1)
  except KeyboardInterrupt:
    print("Interrupted")
    cleanup()