import math
import Queue
import time
import os.path
import json

def isDebugging():
  return os.path.isfile("gps_debugging.json")

def debuggingData():
  positions = False
  if os.path.isfile("gps_debugging.json"):
    with open("gps_debugging.json") as f:
      positions = json.load(f)
  return positions


def metersBetweenPoints(loc1, loc2):
  lat1 = loc1['lat']
  lon1 = loc1['lon']
  lat2 = loc2['lat']
  lon2 = loc2['lon']
  # /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
  # /*  Latitude/longitude spherical geodesy formulae & scripts           (c) Chris Veness 2002-2015  */
  # /*   - www.movable-type.co.uk/scripts/latlong.html                                   MIT Licence  */
  # /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
  R = 6371000 # metres
  q1 = math.radians(lat1)
  q2 = math.radians(lat2)
  dq = math.radians(lat2-lat1)
  dl = math.radians(lon2-lon1)
  a = math.sin(dq/2) * math.sin(dq/2) + math.cos(q1) * math.cos(q2) * math.sin(dl/2) * math.sin(dl/2)
  c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
  d = R * c
  return d


def coordinatesMinutesToDecimal(lat, lon):
  """
    Example lat,lon input:
    -3355.8407, 1852.3123
    Example lat,lon output:
    -33.9304466667, 18.8719533333
  """
  lat = lat/100
  lon = lon/100

  # Split decimal part from integer part
  lat_decimal, lat_integer = math.modf(lat)
  lon_decimal, lon_integer = math.modf(lon)

  # Conver minutes to decimal
  lat = lat_integer + lat_decimal*100.0/60.0
  lon = lon_integer + lon_decimal*100.0/60.0
  return lat, lon

def stripCoordinates(nmea):
  """
    Example nmea inputs:
    $GPGGA,161154.000,3355.8373,S,01852.3155,E,1,04,3.3,121.6,M,32.7,M,,0000*42
    $GPRMC,161153.000,A,3355.8373,S,01852.3155,E,0.11,142.72,170815,,*14
  """
  code = nmea[:6]
  content = nmea.split(',')
  if code == "$GPGGA":
    lat = float(content[2])
    lon = float(content[4])
    lat = (lat if content[3] == "N" else -lat)
    lon = (lon if content[5] == "E" else -lon)
  if code == "$GPRMC":
    lat = float(content[3])
    lon = float(content[5])
    lat = (lat if content[4] == "N" else -lat)
    lon = (lon if content[6] == "E" else -lon)
  return coordinatesMinutesToDecimal(lat, lon)

def containsCoordinates(nmea):
  """
    Example nmea inputs:
    $GPGGA,161154.000,3355.8373,S,01852.3155,E,1,04,3.3,121.6,M,32.7,M,,0000*42
    $GPRMC,161153.000,A,3355.8373,S,01852.3155,E,0.11,142.72,170815,,*14
  """
  code = nmea[:6]
  if code == "$GPGGA" or code == "$GPRMC":
    return True
  else:
    return False

def start(queue, loc):
  print "Starting GPS..."

  if isDebugging():
    locations = debuggingData()
    while 1:
      print "DEBUGGER - GPS: Starting locations loop."
      for l in locations:
        lat = l['lat']
        lon = l['lon']
        loc['lat'] = lat
        loc['lon'] = lon
        queue.put({ "activity": "location",
                    "location": { "latitude": lat, "longitude": lon },
                    "created_at": time.strftime("%Y-%m-%d %H:%M:%S") })
        time.sleep(1)
  else:
    import serial
    connection = serial.Serial('/dev/ttyAMA0', 4800, timeout=1)
    connection.open()
    print "Started..."

    try:
      while 1:
        try:
          response = connection.readline()
          # print response[:6]
          if(containsCoordinates(response)):
            lat, lon = stripCoordinates(response)
            loc['lat'] = lat
            loc['lon'] = lon
            # print "Location: ", lat, lon
            queue.put({ "activity": "location",
                        "location": { "latitude": lat, "longitude": lon },
                        "created_at": time.strftime("%Y-%m-%d %H:%M:%S") })
        except ValueError:
          print "ERROR: GPS value error occurred. Probably no lock."
    except KeyboardInterrupt:
      connection.close()
