import requests
import json
import os
import Queue

# Configure server address
server_address = "https://skripsie.herokuapp.com"
if os.path.isfile("server_address.txt"):
  server_address = open("server_address.txt").read().strip()
  print "Server address: " + server_address
else:
  print "Using default server address."

def handshake():
  _personality = {}
  print "Checking if drone has a personality..."
  while True:
    if os.path.isfile("personality.json"):
      with open("personality.json") as f:
        _personality = json.load(f)
      if _personality["name"]:
        print "Personality confirmed. Hey, my name is "+_personality["name"]+"!"
        break
    else:
      print "Uhmm... not yet, requesting one now."
      r = requests.get(server_address + "/device/subscribe")
      data = r.json
      data["drone_id"] = data["id"]
      with open("personality.json", 'w') as f:
        json.dump(data, f)
  return _personality

def downloadFlightplan(flightplan, personality):
  """
  Keep flight plan updated.
  """
  fp = {}
  try:
    r = requests.get(server_address + "/device/" + str(personality["drone_id"]) + "/flightplan")
    fp = r.json
    print "Flightplan downloaded."
    with open("flightplan.json", 'w') as f:
      json.dump(fp, f)
  except:
    print "Could not download flightplan."
    if os.path.isfile("flightplan.json"):
      with open("flightplan.json") as f:
        fp = json.load(f)
  return fp

def processQueue(queue, personality):
  """
  This function offloads the queue, process it,
  and then uploads everything to the web server.

  Images are loaded into files-object and then
  uploaded with the rest of the data.
  """
  items = []
  files = {}

  # Offload queue into array
  while queue.qsize() > 0:
    i = queue.get()
    items.append(i)

  print "Queue size: " + str(len(items))
  # Read actual images files
  for item in items:
    if(item["activity"] == "photo"):
      files[item["path"].replace("photos/", "")] = open(item["path"], 'rb')

  try:
    # Now upload all the data
    data_url = server_address+"/device/"+str(personality["drone_id"])+"/update"
    payload = json.dumps(items)
    r = requests.post(data_url, data=payload, headers={'Content-type': 'application/json', 'Accept': 'text/plain'})
    if files != {}:
      photo_url = server_address+"/device/"+str(personality["drone_id"])+"/upload"
      r = requests.post(photo_url, files=files)
      # Now delete files from device
      for k, v in files.iteritems():
        os.remove("photos/" + k)
  except:
    print "Urggg, could not connect out to server. Possibly lost signal. Re-adding all to queue."
    for item in items:
      queue.put(item)
