#!/usr/bin/python
# Code Adapted from http://www.raspberrypi-spy.co.uk/2013/10/analogue-sensors-on-the-raspberry-pi-using-an-mcp3008/

# PI Pins: http://www.jameco.com/Jameco/workshop/circuitnotes/raspberry_pi_circuit_note_fig2a.jpg
import spidev
import os
import time

# Open SPI bus
spi = spidev.SpiDev()
spi.open(0, 0)

# Function to read SPI data from MCP3008 chip
# Channel must be an integer 0-7
def ReadChannel(channel):
  adc = spi.xfer2([1,(8+channel)<<4,0])
  data = ((adc[1]&3) << 8) + adc[2]
  return data

# Function to convert data to voltage level (3-decimal places)
def ConvertVolts(data):
  """ Convert read data to voltage physical level
  >>> ConvertVolts(1023)
  3.3
  >>> ConvertVolts(0)
  0.0
  >>> ConvertVolts(500)
  1.613
  """
  volts = (data * 3.3) / float(1023)
  volts = round(volts, 3)
  return volts

# Read channel and convert to volts
# Applying calibration
def ReadVoltsAtChannel(channel):
  data = ReadChannel(channel)
  calibration = [1.0145, 1.1883, 1.2553, 1.2689, 1.4520, 1.4192, 1.4286, 1.4591]
  volts = ConvertVolts(data) * calibration[channel]
  return volts

# Read all 8 channels and return array, unfiltered
def ReadVoltsAtAllChannels():
  channels = range(0, 8)
  voltages = []
  for i in channels:
    volt = ReadVoltsAtChannel(i)
    voltages.append(volt)
  return voltages

def PercentageFromVoltage(voltage, cells):
  """ Calculate battery percentage from total voltage
  >>> PercentageFromVoltage(11.19, 3)
  15.4
  >>> PercentageFromVoltage(18.25, 5)
  1.0
  """
  percentage = (voltage/cells - 164.0/45.0) * 180.0
  return round(percentage, 3)

# Read all 8 channels, calculate total voltage, calculate percentage
def ReadBatteryVoltages():
  voltages = ReadVoltsAtAllChannels()
  zeros = voltages.count(0)
  cells = 8 - zeros
  total_voltage = sum(voltages)
  percentage = PercentageFromVoltage(total_voltage, cells)
  return total_voltage, percentage, voltages

# Main function to use for testing hardware
if __name__ == '__main__':
    print "###################################"
    print "### TESTING INDIVIDUAL CHANNELS ###"
    print "###################################"
    print "Testing channel read on channel 0:", ReadChannel(0)
    print "Testing channel read on channel 1:", ReadChannel(1)
    print "Testing channel read on channel 2:", ReadChannel(2)
    print "Testing channel read on channel 3:", ReadChannel(3)
    print "Testing channel read on channel 4:", ReadChannel(4)
    print "Testing channel read on channel 5:", ReadChannel(5)
    print "Testing channel read on channel 6:", ReadChannel(6)
    print "Testing channel read on channel 7:", ReadChannel(7)
    print "###################################"
    print "#### TESTING ALL CHANNELS READ ####"
    print "###################################"
    print "All channels's volts:", ReadVoltsAtAllChannels()
    print "###################################"
    print "## TESTING INDIVIDUAL CONSISTENCY #"
    print "###################################"
    for c in range(0, 7):
      print "CHANNEL", c
      for i in range(0, 10):
        print "Reading: ", ReadChannel(c)
      print "--------------------------"


