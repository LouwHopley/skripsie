# A generic API to interface with multiple different types of drones.

def getStatus():
  """
  Returns the status of the drone as a string. Possible statusses:
  offline, moving, hovering, crashed, landed
  """
  print "DRONE INTERFACE: Retrieving status."
  # TODO
  return "online"

def goTo(latitude, longitude, altitude):
  """
  Sends the drone to that certain position at
  a certain altitude in feet.
  """
  # TODO
  print "DRONE INTERFACE: Going to new location."
  return True

def hover(altitude):
  """
  Sets the drone in a hover position at a certain altitude in feet
  holding its position in the X, Y & Z directions.
  """
  # TODO
  print "DRONE INTERFACE: Holding position."
  return True