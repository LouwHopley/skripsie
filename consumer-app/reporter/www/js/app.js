angular.module('starter', ['ionic', 'ngIOS9UIWebViewPatch', 'ngCordova'])

.run(function($ionicPlatform, $ionicLoading) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'templates/home.html'
    })
    .state('account', {
      url: '/account',
      templateUrl: 'templates/account.html'
    })
    .state('emergencies', {
      url: '/emergencies',
      templateUrl: 'templates/emergencies.html',
      resolve: {
        emergencyPromise: function(emergencies) {
          return emergencies.getAll().then();
        }
      }
    })
    .state('emergency', {
      url: '/emergencies/{id}',
      templateUrl: 'templates/emergency.html',
      resolve: {
        emergencyPromise: function($stateParams, emergencies) {
          return emergencies.get($stateParams.id);
        }
      },
      controller: function($scope, emergencyPromise){
          $scope.emergency = emergencyPromise;
      }
    });
  $urlRouterProvider.otherwise('/');
});