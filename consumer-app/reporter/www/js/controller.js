angular.module('starter')
.controller('ReportCtrl', function($scope, $state, $ionicModal, user, emergencies, $ionicSideMenuDelegate, $ionicPopup, $ionicHistory, $ionicLoading, $cordovaGeolocation) {
    $scope.emergencies = emergencies.emergencies;

    if($scope.emergency)
    {
      console.log("Added to scope!");
      console.log($scope.emergency);
    }
    // State change event listener
    $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
      if(toState["name"] === "account")
      {
        $scope.user = user.get();
      }
      else
      {
        if(!user.hasAccount())
        {
          // Redirect to force account setup if not done yet
          $state.go("account");
        }
      }
    });

  $scope.updateAccount = function(u) {
    if(typeof u !== "undefined" && "name" in u && "number" in u)
    {
      // Store user's name & number
      user.name(u["name"]);
      user.number(u["number"]);
      $ionicPopup.alert({
        title: 'Changes Saved',
        template: 'Your info has been successfully updated.'
      }).then(function(result) {
        $state.go("home");
      });
    }
    else {
      $ionicPopup.alert({ title: 'Error!', template: 'Please fill in all the fields.' });
    }
  }

  // Create our modal
  $ionicModal.fromTemplateUrl('new-emergency.html', function(modal) {
    $scope.emergencyModal = modal;
  }, {
    scope: $scope
  });

  $scope.createEmergency = function(emergency, name, phone) {
    if(!emergency) {
      console.error("Need to give info");
      return;
    }
    $scope.emergencyModal.hide();

    $ionicLoading.show({ template: 'Reporting...' });
    var em = {};
    angular.copy(emergency, em)
    console.log("Starting location fetch...");
    navigator.geolocation.getCurrentPosition(function(position){
      var latitude = position.coords.latitude;
      var longitude = position.coords.longitude;
      console.log("Location locked: lat: "+latitude+"\tlon: "+longitude);
      em.latitude = latitude;
      em.longitude = longitude;
      em.name = window.localStorage['user.name'];
      em.number = window.localStorage['user.number'];
      em.services_required = $scope.services_required;

      // Create actual emergency
      emergencies.create(em).then(function(res){
        // Route to / view emergency
        $state.go("emergency", {id: res.data.id});
        $ionicLoading.hide();
      });

    }, function(error){
      console.error(error);
      $ionicLoading.hide();
    });

    // Cleanup
    emergency.description = "";
  };

  $scope.openEmergency = function(emergency) {
    console.log("Opening emergency: "+ emergency.id);
    $state.go("emergency", {id: emergency.id});
  };

  $scope.newEmergency = function(a) {
    console.log("New: " + a);
    $scope.services_required = a;
    $scope.emergencyModal.show();
  };

  $scope.closeNewEmergency = function() {
    $scope.emergencyModal.hide();
  }

  $scope.toggleMenu = function() {
    $ionicSideMenuDelegate.toggleRight();
  };

});
