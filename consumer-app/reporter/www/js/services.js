angular.module('starter')
.factory('user', function() {
  return {
    name: function(a) {
      if(typeof a === "undefined")
        return window.localStorage['user.name'];
      else
      {
        window.localStorage['user.name'] = a;
        return a;
      }
    },
    number: function(a) {
      if(typeof a === "undefined")
        return window.localStorage['user.number'];
      else
      {
        window.localStorage['user.number'] = a;
        return a;
      }
    },
    hasAccount: function() {
      if((window.localStorage['user.name'] || '') !== '' && (window.localStorage['user.number'] || '') !== '')
        return true;
      else
        return false;
    },
    get: function() {
      return {
        name: window.localStorage['user.name'],
        number: window.localStorage['user.number']
      }
    }
  };
})
.factory('emergencies', function($http, $ionicLoading) {
  // var host = "http://192.168.8.100:3000"; // Office Wifi
  // var host = "http://10.110.29.165:3000"; // Maties Wifi
  // var host = "http://172.20.10.3:3000"; // Personal hotspot
  var host = "http://skripsie.herokuapp.com";
  var o = { emergencies: [] };
  o.getAll = function() {
    var name = window.localStorage['user.name'];
    var number = window.localStorage['user.number'];
    console.log("Getting all emergencies.");
    $ionicLoading.show({ template: "<ion-spinner icon='ios'></ion-spinner><br>Loading" });
    return $http.get(host + '/app/emergencies/' + encodeURIComponent(number) + '/' + encodeURIComponent(name) + '.json').success(function(data){
      $ionicLoading.hide();
       angular.copy(data, o.emergencies);
    });
  };
  o.get = function(id) {
    var name = window.localStorage['user.name'];
    var number = window.localStorage['user.number'];
    $ionicLoading.show({ template: "<ion-spinner icon='ios'></ion-spinner><br>Loading" });
    return $http.get(host + '/app/emergencies/' + encodeURIComponent(number) + '/' + encodeURIComponent(name) + '/' + id + '.json').then(function(res){
      $ionicLoading.hide();
      return res.data;
    });
  };
  o.create = function(emergency) {
    console.log("Creating emergency...")
    return $http.get(host + '/app/emergencies/create', {params: emergency}).then(function(success) {
      console.log("Emergency created.");
      return success;
    }).catch(function(error) {
      console.log("Eish!", error);
    });
  };
  return o;
})
.factory('emergency', function() {
  var o = {emergency: {}};
  return o;
});